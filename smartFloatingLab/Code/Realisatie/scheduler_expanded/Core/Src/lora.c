/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "debug.h"
#include "lora.h"

#include <main.h>
#include <string.h>
#include "rmc.h"


/* USER CODE BEGIN Includes */

#include <stdio.h>
#include "SX1278.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
/* USER CODE END PV */
void MX_GPIO_Init(void);
void MX_SPI1_Init(void);
void MX_USART6_UART_Init(void);
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/


SX1278_hw_t SX1278_hw;
SX1278_t SX1278;
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */


/*
Description:	Inits the LoRa module IO, sets frequency to 868MHZ, 17DMB signal, SF8 and bandwidth 125kHz.
Input:			None.
*/
void initLora(void)
{
	SX1278_hw.dio0.port = DIO0_GPIO_Port;
	SX1278_hw.dio0.pin = DIO0_Pin;
	SX1278_hw.nss.port = NSS_GPIO_Port;
	SX1278_hw.nss.pin = NSS_Pin;
	SX1278_hw.reset.port = RESET_LORA_GPIO_Port;
	SX1278_hw.reset.pin = RESET_LORA_Pin;
	SX1278_hw.spi = &hspi1;

	SX1278.hw = &SX1278_hw;

	debugCharPrint("Configuring LoRa module\r\n");
	SX1278_begin(&SX1278, SX1278_868_5MHZ, SX1278_POWER_17DBM, SX1278_LORA_SF_8, SX1278_LORA_BW_125KHZ,100);
	debugCharPrint("Done configuring LoRaModule\r\n");
}
/*
Description:	Sets the module to receiver mode.
Input:			None.
*/
void initReceive(void)
{
	ret = SX1278_LoRaEntryRx(&SX1278, 100, 2000);
}
/*
Description:	Receives and returns the payload. It prints the sent parameters and their values.
				Checks the CRC and prints wether it is good or not.
Input:			None.
Output:			char* to the received payload string.
*/
char* receiveString(void)
{
	//Read the IO pin. High -> package received. Low -> no package received.
	ret = SX1278_LoRaRxPacket(&SX1278);

	if (ret > 0) //Code to be executed only if a package is received
	{
		debugCharPrint("Package received ...");
		debugPrintEnter();
		SX1278_read(&SX1278, (uint8_t*) payloadBuffer, ret); //read the payload from the module and save it in a buffer.
		debugCharPrint("Bytes received: ");
		debugIntPrint(ret);
		debugPrintEnter();
		int offset = 0;
		char value[10];
		char parameters[22][20] = {"Distance:", "Bearing:", "COG:", "Lat:", "Lon:", "Pterm:", "Iterm:", "Dterm:", "errorAngle:", "PIDperc:", "MotorL:", "MotorR:", "Servo:", "SampleTime:", "Eindbestemming:", "Bestemming:", "Battery voltage:", "Terugroep:", "Temperatuur:", "UltrasoonLinks:", "UltrasoonRechts:", "Wachttijd:"};
				for(int i = 0; i<22; i++) //loop through the payload untill a ' '(spacebar) is found (payload is spacebar seperated). Print the parameter name followed by the value.
				{
					offset = offset + copyData(payloadBuffer+offset,value,' '); //copies the data in the buffer and puts it in value intill a '' is found.

					debugCharPrint(parameters[i]);
					debugCharPrint(value);
					debugPrintEnter();


				}

		HAL_GPIO_TogglePin(LED_D1_GPIO_Port, LED_D1_Pin); //Toggle LED_D1 on the PCB to inform the user a package has been received.

		//check the CRC
		if (SX1278_SPIRead(&SX1278, LR_RegHopChannel) && 0x20) //if a message with crc is received
		{
			if (SX1278_SPIRead(&SX1278, LR_RegIrqFlags) && 0x20) //if the PayloadCrcError is high
			{
				debugCharPrint("CRC_ERROR");
				SX1278_SPIWrite(&SX1278, LR_RegIrqFlags, 0x20); //clear the flag
			}
			else
			{
				debugCharPrint("CRC_GOOD");
			}
			debugPrintEnter();debugPrintEnter();
			char *ptr = payloadBuffer;
			return ptr; //only return a ptr if the CRC is good.
		}
	}
	return 0;
}

/*
Description:	Sends a global buffer string over LoRa. Prints wether the transmission was succesful or not.
Input:			None.
Output:			None.
*/
void sendString(char message[])
{
			message_length = sprintf(buffer, message); //Determine the length of the buffer and put buffer in message.
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET); //Sets the module IO in Tx mode.
			ret = SX1278_LoRaEntryTx(&SX1278, message_length, timeOut); //Configures Tx mode.
			debugCharPrint("Entry: ");
			debugCharPrint("Sending ");
			debugCharPrint(buffer);
			debugPrintEnter();

			ret = SX1278_LoRaTxPacket(&SX1278, (uint8_t *) buffer, message_length, timeOut); //Sends the buffer over LoRa. 


			if (ret) //If ret=1 -> transmission successful
			{
				debugCharPrint("Transmission successful...");
				HAL_GPIO_TogglePin(LED_D1_GPIO_Port, LED_D1_Pin);
			}
			else
			{
				debugCharPrint("Transmission failed...");

			}
			debugPrintEnter();debugPrintEnter();
}
/*
Description:	Converts the payload struct into a suitable payload string.
Input:			Pointer to a payload struct.
Output:			None.
*/
void payload2string(payloadTag *payload)
{
	char tmpstr[20];
	memset(&payloadString[0], 0, sizeof(payloadString)); //clears the payloadstring
	
	/*The following lines convert all payload elements from the struct into a payloadstring by converting eache element into a string and concatinating them.
	All elements are seperated by a spacebar ' ' character. 	
	*/
	sprintf(tmpstr, "%d ", payload->distance);	
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->bearing);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->cog);
	strcat(payloadString, tmpstr);

	sprintf(tmpstr, "%.09f ", payload->lat);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.09f ", payload->lon);
	strcat(payloadString, tmpstr);

	sprintf(tmpstr, "%.03f ", payload->PTerm);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->ITerm);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->DTerm);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->errorAngle);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->PIDperc);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->MotorL);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->MotorR);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->Servo);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->SampleTime);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->destination);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->arrived_spots);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.02f ", payload->vBat);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->terugroep);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->temp);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.02f ", payload->UltraDistanceLinks);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.02f ", payload->UltraDistanceRechts);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->counter);
	strcat(payloadString, tmpstr);

}
/*
Description:	Converts the payload struct into a suitable setting string.
Input:			Pointer to a payload struct.
Output:			None.
*/
void setting2string(payloadTag *payload)
{
	/*The following lines convert all setting elements from the struct into a payloadstring by converting eache element into a string and concatinating them.
	All elements are seperated by a spacebar ' ' character. 	
	*/
	char tmpstr[20];
	memset(&settingString[0], 0, sizeof(settingString));

	sprintf(tmpstr, "%d ", payload->reset);
	strcat(settingString, tmpstr);
	sprintf(tmpstr, "%d ", payload->callback);
	strcat(settingString, tmpstr);
	sprintf(tmpstr, "%d ", payload->variable);
	strcat(settingString, tmpstr);

	sprintf(tmpstr, "%.04f ", payload->value);
	strcat(settingString, tmpstr);
}
/*
Description:	Receives the settings and applies them to the code.
Input:			None.
Output:			None.
*/
void receiveSettings(void)
{
	//Checks if a package is received.
	ret = SX1278_LoRaRxPacket(&SX1278);
	
	if (ret > 0) //if a package was received
	{
		debugCharPrint("Package received ...");
		debugPrintEnter();
		SX1278_read(&SX1278, (uint8_t*) settingsBuffer, ret); //Read the package form the LoRa module
		debugCharPrint("Bytes received: ");
		debugIntPrint(ret);
		debugPrintEnter();

		int offset = 0;
		char value[10];
		char parameters[4][13] = { "Reset: ", " Callback: ", " Variable: ", " Value: "};

		for (int i = 0; i < 4; i++) //Loop through the package. Convert the string elements into integers or floats and apply them back into the payload struct.
		{
			offset = offset + copyData(settingsBuffer + offset, value, ' ');
			debugCharPrint(parameters[i]);
			debugCharPrint(value);
			if (i == 0) {
				payload.reset = atoi(value);
			}
			else if (i == 1) {
				payload.callback = atoi(value);
			}
			else if (i == 2) {
				payload.variable = atoi(value);
			}
			else if (i == 3) {
				payload.value = atof(value);
			}
		}
		debugPrintEnter();debugPrintEnter();
	}

}
