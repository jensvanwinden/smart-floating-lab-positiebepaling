/*
* 	File: PID_Test.c
* 	Version:1.0
*  	Author: Chiel-Jan Harleman
*  	Description: Deze library zorgt ervoor dat d.m.v. de gemeten gpsdata en orientatiedata, de koers bepaald kan worden.
*  	De library zorgt er tevens voor dat de data wordt omgezet wordt in een respons van de motoren.
*  	Dit is de H-file van de library.
*/


#ifndef SRC_PID_TEST_C_
#define SRC_PID_TEST_C_

#include "main.h"
#include <stdio.h>
#include <math.h>
#include "PID_Test.h"
#include "debug.h"
#include "stdlib.h"
#include "lora.h"


float Kp = 2.0;
float KiSet = 1.0;
float Ki = 0.005;
float Kd = 2.5;
#define maxITerm 150.0
#define minITerm -150.0
#define maxDTerm 25.0
#define minDTerm -25.0
#define maxPTerm 500
#define minPTerm -500
#define PWMPeriod 2000.0
#define invertSteering 0

float lastError = 0;
int8_t engineSteering = 1;
float ITerm = 0;

/*
 * Deze functie zorgt ervoor dat de hoek die mee wordt gegeven, genormaliseerd wordt van 0 tot 360 graden.
 */
int16_t setTo360(int16_t degree)
{
	if(degree < 0)
	{
		degree = degree + 360;
	}

	return degree;
}
/*
 * deze functie zorgt ervoor dat een berekende hoek omgezet wordt naar een percentage waar de regelaar mee kan rekenen.
 */
float degreeToPercentage(int16_t degree)
{
	float degreeF = (float)degree;
	float percentage = degreeF * (100.0 / 180.0);

	return percentage;
}

/*
 *Deze functie zorgt ervoor dat de kleinste errorhoek bepaald kan worden aan de hand van de ingegeven hoeken.
 */
int16_t determineError(int16_t setpointAngle, int16_t orientationAngle)
{
	int16_t errorAngle = 0;
	int16_t angle1	= 0;
	int16_t angle2 = 0;
	errorAngle = setTo360(setpointAngle) - setTo360(orientationAngle);

	if(errorAngle < -180 || errorAngle > 180)
	{
		angle1 = errorAngle + 360;
		angle2 = errorAngle - 360;

		if(pow(angle1,2) > pow(angle2,2))
		{
			payload.errorAngle = angle2;
			return angle2;
		}
		else
		{
			payload.errorAngle = angle1;
			return angle1;
		}
	}
	else
	{
		payload.errorAngle = errorAngle;
		return errorAngle;
	}
}
/*
 * Deze functie zorgt ervoor dat er uit een ingevoerde hoeken (Bearing en COG) een koers kan worden berekend.
 * De functie zorgt er ook voor dat het berekende percentage direct de motoren aanstuurt.
 *
 */
void calculatePID(float bearing, char* cog)
{
	static float newError = 0;

	float DTerm = 0;
	float PTerm = 0;
	float totalGain = 0;

	int16_t setPoint = (int16_t)bearing;
    int16_t	orientatie = atoi(cog);

	payload.bearing = setTo360(setPoint);
	payload.cog = setTo360(orientatie);

    newError = degreeToPercentage(determineError(setPoint, orientatie));  				// with bearing and cog

    extern float newTime;
	extern float lastTime;
	float sampleTimeF = newTime - lastTime;
	payload.SampleTime = sampleTimeF;

	PTerm =  	Kp*(newError * sampleTimeF);
	DTerm = 	Kd*((lastError - newError) / sampleTimeF);
	ITerm += 	Ki*((KiSet*newError) * sampleTimeF);


	if ((lastError - newError) > 150 || (lastError - newError) < -150)
	{
		DTerm = 0;
	}

	if(DTerm > maxDTerm)
	{
		DTerm = maxDTerm;
	}
	else if(DTerm < minDTerm)
	{
		DTerm = minDTerm;
	}

	if(PTerm > maxPTerm)
	{
		PTerm = maxPTerm;
	}
	else if(PTerm < minPTerm)
	{
		PTerm = minPTerm;
	}

	if(ITerm > maxITerm)
	{
		ITerm = maxITerm;
	}
	else if(ITerm < minITerm)
	{
		ITerm = minITerm;
	}

	totalGain = PTerm + DTerm + ITerm;

		if(invertSteering == 1)
		{
			totalGain = totalGain*-1;
		}

	payload.PIDperc = totalGain;

	if((totalGain > 100) && (lastError > 0) && (newError > 0))
	{
		KiSet = 0.0;
	}
	else if((totalGain < -100) && (lastError < 0) && (newError < 0))
	{
		KiSet = 0.0;
	}
	else
	{
		KiSet = 1.0;
	}

	payload.PTerm = PTerm;
	payload.ITerm = ITerm;
	payload.DTerm = DTerm;

	percentageToPWMvoid(totalGain);
	lastTime = newTime;
	lastError = newError;
}

/*
 *Deze functie zorgt ervoor dat het percentage dat berekend wordt door de regelaar omgezet wordt naar een PWM signaal
 *voor de motoren.
 */
void percentageToPWMvoid(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;
	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod);

	if(engineSteering == 1)
	{

		if(PIDPercentage > 100)
		{
			PIDPercentage = 100;
		}

		else if (PIDPercentage < -100)
		{
			PIDPercentage = -100;
		}

		if(PIDPercentage <= 0) //links sturen
		{
			rightEngine = PWMPeriod*enginePowerLimit.enginePowerLimitRight;
			leftEngine = (PWMPeriod*enginePowerLimit.enginePowerLimitLeft) + (fabs(PIDPercentage)*enginePowerLimit.engineDivideFactorLeft); //
			payload.MotorL = leftEngine;
			payload.MotorR = rightEngine;
		}

		else if(PIDPercentage > 0) //rechts sturen
		{
			leftEngine = PWMPeriod*enginePowerLimit.enginePowerLimitLeft;
			rightEngine = (PWMPeriod*enginePowerLimit.enginePowerLimitRight) + (fabs(PIDPercentage)*enginePowerLimit.engineDivideFactorRight); //
			payload.MotorL = leftEngine;
			payload.MotorR = rightEngine;
		}
	}
	else
	{
		engineMode(forwardHalfThrottle);
	}

	uint16_t PWMTimeI = (uint16_t)PWMTimeF;

	if(PWMTimeI > 200)
	{
		PWMTimeI = 200;
		servo = PWMTimeI;
		payload.Servo = PWMTimeI;
	}
	else if(PWMTimeI < 100)
	{
		PWMTimeI = 100;
		servo = PWMTimeI;
		payload.Servo = PWMTimeI;
	}
	else
	{
		servo = PWMTimeI;
		payload.Servo = PWMTimeI;
	}


}
/*
 * Deze functie zorgt ervoor dat het percentage dat berekend wordt door de regelaar omgezet wordt naar een PWM waarde in een integer.
 */
int16_t percentageToPWM(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;

	if(PIDPercentage > 100)
	{
		PIDPercentage = 100;
	}
	if(PIDPercentage < -100)
	{
		PIDPercentage =-100;
	}

	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	int16_t PWMTimeI = (int16_t)PWMTimeF;
	return PWMTimeI;
}
/*
 * Deze functie wordt gebruikt om de motoren te testen, de unittest uit te voeren en om de boot tijdens obstakeldetectie achteruit te laten
 * varen.
 * Iedere case in deze functie stuurt de motoren zo aan als de naam aangeeft.
 */
void engineMode(uint8_t engineMode)
{
	switch(engineMode)
	{
		case forward:
			{
				rightEngine= PWMPeriod*enginePowerLimit.enginePowerLimitRight; //links
				leftEngine= PWMPeriod*enginePowerLimit.enginePowerLimitLeft; //rechts
			}
		break;
		case forwardHalfThrottle:
			{
				rightEngine= PWMPeriod*0.0625; //links
				leftEngine= PWMPeriod*0.0625; //rechts
			}
		break;
		case off:
		{
				rightEngine= PWMPeriod*0.075;
				leftEngine= PWMPeriod*0.075;
		}
		break;
		case backward:
		{
				rightEngine= PWMPeriod*0.1;
				leftEngine= PWMPeriod*0.1;
		}
				break;
		case leftTurn:								//Debug case
		{
			rightEngine= PWMPeriod*0.075;
			leftEngine= PWMPeriod*0.1;
		}
		break;
		case rightTurn:								//Debug case
		{
			rightEngine= PWMPeriod*0.1;
			leftEngine= PWMPeriod*0.075;
		}
		break;
	}
}
/*
 * Deze functie zorgt ervoor dat de waarden waarmee de regelaar een aanstuurpercentage bepaalt doorberekend worden
 * wanneer er een limiet wordt ingesteld voor de motoren. Tevens limiteert deze functie het vermogen van de motoren.
 */
void defineEnginePower(int8_t percentageRight, int8_t percentageLeft)
{
	enginePowerLimit.enginePowerLimitRight = (percentageRight * -0.00025) + 0.075;
	enginePowerLimit.engineDivideFactorRight = (150-(enginePowerLimit.enginePowerLimitRight*PWMPeriod))/100;

	enginePowerLimit.enginePowerLimitLeft = (percentageLeft * -0.00025) + 0.075;
	enginePowerLimit.engineDivideFactorLeft = (150-(enginePowerLimit.enginePowerLimitLeft*PWMPeriod))/100;
}
/*
 * De functie zorgt ervoor dat de ESC's en roer in de neutrale modus staan  bij het opstarten
 * In deze functie wordt ook het limiet van het motorvermogen bepaald.
 */
void initEngine()
{
	defineEnginePower(70, 100);
	rightEngine = 150; //links
	leftEngine = 150; //rechts
	servo = 150; //Servo
	payload.MotorL = leftEngine;
	payload.MotorR = rightEngine;
	payload.Servo = servo;
	HAL_Delay(2000);
}

#endif /* SRC_PID_TEST_C_ */
