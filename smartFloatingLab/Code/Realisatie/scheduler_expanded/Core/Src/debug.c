/*
* 	File: debug.c
* 	Version:1.0
*  	Author: Michael van der Bend, Jens van Winden, Vince de Brabander en Chiel-Jan Harleman
*  	Description: Deze library is gebruikt om tijdens het programmeren debugdata via de uart uit te kunnen lezen.
*  	Dit is de C-file van de library.
*/

#include <debug.h>
#include <lora.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>



/*
  * Deze functie wordt gebruikt om de debuguartlijn te initialiseren.
  */
void MX_USART6_UART_Init(void)
{
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 9600;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
}

/*
 * Deze functie wordt gebruikt om een character over uart te verzenden
 */
void debugCharPrint(char sendMessage[])
{
	char newline[2] = "\r\n";
	HAL_UART_Transmit(&huart6, (uint8_t*) sendMessage, strlen(sendMessage), HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart6, (uint8_t*) newline, 2, HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart6, (uint8_t*) newline, 2, HAL_MAX_DELAY);
}
/*
 * Deze functie wordt gebruikt om een integer over uart te verzenden
 */
void debugIntPrint(int message)
{
	char buffer[20] = {0};
	itoa(message,buffer,10);
	debugCharPrint(buffer);
}

/*
 * Deze functie wordt gebruikt om naar de volgende lijn te gaan. Een enter te printen
 */
void debugPrintEnter(void)
{
	char newline[2] = "\r\n";
	HAL_UART_Transmit(&huart6, (uint8_t*) newline, 2, HAL_MAX_DELAY);
}

/*
 * Deze functie wordt gebruikt om een floating point te verzenden over uart.
 */
void debugFloatPrint(float message)
{
	char buffer[10] = {0};
	gcvt(message, 5, buffer);
	//sprintf(buffer, "%g", message);
	debugCharPrint(buffer);
}

