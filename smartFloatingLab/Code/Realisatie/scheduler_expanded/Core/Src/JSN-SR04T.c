/*
 * rmc.c
 *
 *  Created on: 22 nov. 2019
 *      Author: Jens
 */

#include "JSN-SR04T.h"
#include <stdio.h>

void pulseout(void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
	TIM9->CNT = 0;
	while(TIM9->CNT < 10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
}
