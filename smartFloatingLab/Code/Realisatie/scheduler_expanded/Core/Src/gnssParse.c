/*
* File: gnssParse.c
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat alle benodigde data uit de GNSS module wordt
* opgeslagen in het juiste struct. Dit is de c-file van deze libary.
*/

#include <gnssParse.h>
#include "lora.h"

/*
 * Deze functie zorgt ervoor dat alle data uit de geparsde nmea-sentence wordt opgeslagen.
 * Ook wordt de bearing en distance berekent en worden er variabelen gekoppeld aan de LoRa payload.
 * Deze functie returned de data uit de GNSS module.
 */
RMC getGnssData(RMC data, Route *route)
{
	char in[144] = {0};
  	getRawRMC(in);
  	data = parseRMC(in);
	data.bearing = getBearing(data.latConv, data.lonConv, route->currentLocation->lat, route->currentLocation->lon);
	data.haversineDist = getDistance(data.latConv, data.lonConv, route->currentLocation->lat, route->currentLocation->lon);
	payload.lat = data.latConv;
	payload.lon = data.lonConv;
	payload.distance = data.haversineDist;
	return data;
}
