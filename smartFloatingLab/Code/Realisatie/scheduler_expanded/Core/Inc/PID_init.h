/*
* 	File: PID_init.h
* 	Version:1.0
*  	Author: Chiel-Jan Harleman
*  	Description: Deze library zorgt ervoor dat de timers worden ingesteld, dat de pwm
*  	signalen worden geïnitaliseerd en gestart. Dit is de H-file van de library.
*/

#ifndef SRC_PID_INIT_H_
#define SRC_PID_INIT_H_

#include "main.h"

TIM_HandleTypeDef htim5;
void MX_TIM5_Init(void);
void PID_init(void);

#endif /* SRC_PID_INIT_H_ */
