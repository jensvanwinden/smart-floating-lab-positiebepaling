/*
* File: route.h
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een route wordt aangemaakt en locaties
* kunnen worden toegevoegd m.b.v. een gelinkte lijst. Wanneer er een locaties bereikt is
* zal de volgende locatie de huidige worden. Dit is de h-file van deze libary.
*/

#ifndef ROUTE_H_
#define ROUTE_H_

#include <stdint.h>

typedef struct LocationTag
{
	float lat;
	float lon;
	struct LocationTag *next;
}Location;

typedef struct
{
	Location *currentLocation;
	Location *end;
}Route;

Route *createRoute(void);
void addLocation(Route *actual, float lat, float lon);
uint8_t arrived(Route *route);


#endif /* ROUTE_H_ */
