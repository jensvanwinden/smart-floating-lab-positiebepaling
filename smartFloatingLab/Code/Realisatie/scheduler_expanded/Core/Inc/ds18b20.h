/*
* File: ds18b20.h
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een temperatuur gemeten en
* uitgelezen kan worden met een ds18b20 sensor. Dit is de h-file van de library.
*/

#ifndef SRC_DS18B20_H_
#define SRC_DS18B20_H_

#include <stdio.h>
#include "main.h"

TIM_HandleTypeDef htim6;

void MX_TIM6_Init(void);
void DS18B20_mode(int mode);
uint8_t temp_init(void);
void write1(void);
void write0(void);
uint8_t readbit(void);
void writebyte(uint8_t value);
uint8_t readbyte(void);
void convertT(void);
uint16_t readscratchpad(void);
void writescratchpad(void);
void gettemp(void);
uint16_t readtemp(void);

#endif /* SRC_DS18B20_H_ */
