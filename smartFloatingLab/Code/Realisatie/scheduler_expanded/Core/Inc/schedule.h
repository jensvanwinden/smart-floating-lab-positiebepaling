/*
* File: schedule.h
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een taak kan worden uitgevoerd en
* dat de volgende taak wordt bepaald a.d.h.v. de hoogste prioriteit. Dit is de h-file van deze libary.
*/

#ifndef SRC_SCHEDULE_H_
#define SRC_SCHEDULE_H_

#include <task.h>

void sleep(void);
Tasklist *nextTask(Tasklist *list);
void runCurrentTask(Task *task);

#endif /* SRC_SCHEDULE_H_ */
