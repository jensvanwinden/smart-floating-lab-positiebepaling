/*
* 	File: PID_Test.h
* 	Version:1.0
*  	Author: Chiel-Jan Harleman
*  	Description: Deze library zorgt ervoor dat d.m.v. de gemeten gpsdata en orientatiedata, de koers bepaald kan worden.
*  	De library zorgt er tevens voor dat de data wordt omgezet wordt in een respons van de motoren.
*  	Dit is de H-file van de library.
*/

#ifndef SRC_PID_TEST_H_
#define SRC_PID_TEST_H_

#define leftEngine TIM5->CCR1
#define rightEngine TIM5->CCR2
#define servo TIM5->CCR3

/*
 * Globale struct waarin de defineEnginePower van de motoren in opgeslagen worden, zodat de regelaar deze kan gebruiken.
 */
typedef struct
{
	float enginePowerLimitLeft;
	float engineDivideFactorLeft;
	float enginePowerLimitRight;
	float engineDivideFactorRight;
}enginePower;

enginePower enginePowerLimit;

enum engineDirection {forward, forwardHalfThrottle, off, backward, rightTurn, leftTurn, rightForward, leftForward};

int16_t percentageToPWM(float PIDPercentage);
float degreeToPercentage(int16_t degree);
int16_t setTo360(int16_t degree);
int16_t determineError(int16_t setpointAngle, int16_t orientationAngle);
void percentageToPWMvoid(float PIDPercentage);
void calculatePID(float bearing, char* cog);
void engineMode(uint8_t engineMode);
void initEngine();
void defineEnginePower(int8_t percentageRight, int8_t percentageLeft);

#endif /* SRC_PID_TEST_H_ */
