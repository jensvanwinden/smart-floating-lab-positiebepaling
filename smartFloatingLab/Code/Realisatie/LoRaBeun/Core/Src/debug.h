/*
 * debug.h
 *
 *  Created on: Dec 2, 2019
 *      Author: Vince
 */

#ifndef SRC_DEBUG_H_
#define SRC_DEBUG_H_

#include <main.h>
#include <main.h>



UART_HandleTypeDef huart1;
void MX_USART6_UART_Init(void);
void debugCharPrint(char sendMessage[]);
void debugIntPrint(int message);
void debugPrintEnter(void);
void debugFloatPrint(float message);
void debugRead(void);

#endif /* SRC_DEBUG_H_ */
