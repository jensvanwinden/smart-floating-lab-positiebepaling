/*
 * lora.h
 *
 *  Created on: Dec 20, 2019
 *      Author: Micha
 */

#ifndef SRC_LORA_H_
#define SRC_LORA_H_

typedef struct
{
	//Payload
	int16_t distance;	//Aftstand tot de ingestelde lcoatie
	int16_t bearing;	
	int16_t cog;		//Course over ground / vaarrichting
	float lat;			//GPS latitude
	float lon;			//GPS longtitude

	float PTerm;		//Proportienele factor van de regelaar
	float ITerm;		//Itererende factor van de regelaar
	float DTerm;		//Differentiele factor van de regelaar
	float errorAngle;	//Verschil tussen huidige vaarrichting en bestemming
	float PIDperc;		//Uitgang van de PID regelaar
	float MotorL;		//Linker motor 100(vol gas) - 150(stil)
	float MotorR;		//Rechter motor 100(vol gas) - 150(stil)
	int16_t Servo;		//Servo 100(links)-200(rechts)
	float SampleTime;	//tijd tussen 2 PID samples
	int destination;	//Eindbestemming bereikt 1/0
	int arrived_spots;	//Huidige bestemming
	float vBat;			//Batterijspanning
	uint16_t temp;		//Watertemperatuur
	int terugroep;		//Terugroepactie 1/0
	float UltraDistanceLinks;	//Afstand gezien door ultrasone sensor links
	float UltraDistanceRechts;	//Afstand gezien door ultrasone sensor rechts
	float counter;		//Hoe lang het lab op locatie ana het drijven is in seconde
	//Settings
	int reset;			//(NIET GEIMPLEMENTEERD)Optionele parameter om het lab te resetten via LoRa 
	int callback;		//1/0 VOOR HET UITVOEREN VAN TERUGROEPACTIE
	int variable; 		//(NIET GEIMPLEMENTEERD) Optionele parameter voor het wijzigen van PID waardes. nothing = 0 kp=1, ki=2, kd=3
	float value;		//(NIET GEIMPLEMENTEERD) Optionele parameter voor het wijzigen van PID waardes

	int ready;			//

}payloadTag;
payloadTag payload;


void initLora(void);
void initReceive(void);
char *receiveString(void);
char *receiveInt(void);
void sendString(char message[]);
void sendInt(int message[]);
void payload2string(payloadTag *payload);
void setting2string(payloadTag *payload);
void receiveSettings(void);
void loraSendSettings(void);


#define timeOut 2000



int ret;
char buffer[200];
char payloadBuffer[200];
char settingsBuffer[200];

int message_length;
enum type {Sender = 1, Receiver = 0};
enum payloadtype {Settings = 1, Payload = 0};

//Change this when uploading to define sender and receiver PCB


char payloadString[200];
char settingString[200];

#endif /* SRC_LORA_H_ */
