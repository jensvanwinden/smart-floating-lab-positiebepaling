/*
 * tempTest.c
 *
 *  Created on: 21 jan. 2020
 *      Author: Vince
 */
#include "ds18b20.h"
#include "munit/munit.h"

static MunitResult readScratchpad_test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;
    uint8_t data1[] = {0xD0,0x07 };
    uint8_t data2[] = {0x50,0x05 };
    uint8_t data3[] = {0x91,0x01 };
    uint8_t data4[] = {0xA2,0x00 };
    uint8_t data5[] = {0x08,0x00 };
    uint8_t data6[] = {0x00,0x00 };
    uint8_t data7[] = {0x38,0x01 }; //bijbehorend aan de meting van de unittest

    munit_assert_uint16(readscratchpad(data1), ==, 12500);
    munit_assert_uint16(readscratchpad(data2), ==, 8500);
    munit_assert_uint16(readscratchpad(data3), ==, 2500);
    munit_assert_uint16(readscratchpad(data4), ==, 1000);
    munit_assert_uint16(readscratchpad(data5), ==, 50);
    munit_assert_uint16(readscratchpad(data6), ==, 0);
    munit_assert_uint16(readscratchpad(data7), ==, 1950);
	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
	{"/readScratchpad", readScratchpad_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},

    // Always add this one to tell runner testing is over!
    { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

// Main runner that picks which suites we should run
static const MunitSuite test_suite = {
    (char*) "test DS18B20",
    test_suite_tests,
    NULL,
    1,
    MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char *argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
    return munit_suite_main(&test_suite, NULL, argc, argv);
}
