/*
 * ds18b20.c
 *
 *  Created on: 22 nov. 2019
 *      Author: Vince
 */

#include "ds18b20.h"


#define OUT 1
#define IN	0

/*
 * Deze functie zorgt ervoor dat de gemeten temperatuur kan worden uitgelezen.
 * De gemeten temperatuur wordt gereturned.
 */
uint16_t readscratchpad(uint8_t testData[])
{
	uint16_t temp = 0;
	uint16_t ans = 0;
	uint8_t data[9];
//	temp_init();
//	writebyte(0xCC);	//skip rom
//	writebyte(0xBE);	//read scratchpad
	for (int i = 0; i < 9; i++)
	{
		data[i] = testData[i];
	}
	temp = data[1];
	temp <<= 8;
	temp |= data[0];
	ans = ((temp/4)*0.25)*100;
	return ans;
}


