/*
 * PID.h
 *
 *  Created on: 21 jan. 2020
 *      Author: ToBeEdited
 */

#ifndef PID_H_
#define PID_H_

typedef struct
{
	float enginePowerLimitLeft;
	float engineDivideFactorLeft;
	float enginePowerLimitRight;
	float engineDivideFactorRight;
}enginePower;

enginePower enginePowerLimit;

float degreeToPercentage(int degree);
int setTo360(int degree);
int determineError(int setpointAngle, int orientationAngle);
int percentageToPWM(float PIDPercentage);
//void calculatePIDPercentage(float errorPercentage)
void calculatePID();
//void calculatePID(float bearing, char* cog);
void defineEnginePower(int8_t percentageRight, int8_t percentageLeft);

#endif /* PID_H_ */
