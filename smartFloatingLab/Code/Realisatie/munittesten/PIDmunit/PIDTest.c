/*
 * graphTest.c
 *
 *  Created on: 4 okt. 2019
 *      Author: vince
 */

#include <stdio.h>
#include "munit/munit.h"
#include "PID.h"




static MunitResult setTo360_Test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    int testValue = 0;
    int compareValue = 0;

    for(int i = -360; i < 0; i += 10)
    {
    	testValue = setTo360(i);
    	munit_assert_int(testValue , ==, compareValue);
    	compareValue += 10;
    }

	return MUNIT_OK;
}

static MunitResult determineError_Test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    int testSetpoint = 0;
    int testOrientatie = 0;
    int testValue = 0;
    int compareValue = 0;

    	//Test from setpoint 0 degree full range input -360 til 360 degree
        for(testOrientatie = -360; testOrientatie < 360; testOrientatie += 10)
        {
        	testValue = determineError(testSetpoint,testOrientatie);
        	munit_assert_int(testValue , ==, compareValue);

        	//From this point it should be a positive angel for a turn to the opposite side
        	if(compareValue <= -180)
        	{
        		compareValue = 180;
        	}

        	compareValue += -10;
        }

	return MUNIT_OK;
}

static MunitResult degreeToPercentage_Test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    float testValue = 0.0;

    //Test if 180 degree is equal to 100%
    testValue = degreeToPercentage(180);
    munit_assert_double_equal(testValue, 100.0, 3);

    //Test if -180 degree is equal to -100%
    testValue = degreeToPercentage(-180);
    munit_assert_double_equal(testValue, -100.0, 3);

    //Test if 0 degree is equal to 0%
    testValue = degreeToPercentage(0);
    munit_assert_double_equal(testValue, 0.0, 3);

    //Test if 10 degree is equal to 5.555%
    testValue = degreeToPercentage(10);
    munit_assert_double_equal(testValue, 5.555, 3);

    //Test if -10 degree is equal to -5.555%
    testValue = degreeToPercentage(-10);
    munit_assert_double_equal(testValue, -5.555, 3);

	return MUNIT_OK;
}

static MunitResult defineEnginePower_Test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    //Set enginepower both to max = 100%
    defineEnginePower(100,100);

    //Max forward should be a pwm value of 100
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitLeft*2000 ,100.0 , 3);
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitRight*2000 ,100.0 , 3);
    //Max Percentage out of PID should be 0.5*100
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorLeft*100 ,50 , 3);
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorRight*100 ,50 , 3);

    //Set enginepower both to max = 0%
    defineEnginePower(0,0);

    //Min forward should be a pwm value of 150
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitLeft*2000 ,150.0 , 3);
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitRight*2000 ,150.0 , 3);
    //Max Percentage out of PID should be 0.0*100
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorLeft*100 ,0 , 3);
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorRight*100 ,0 , 3);

    //Set enginepower both to max = 50%
    defineEnginePower(50,50);

    //Fifty percent forward should be a pwm value of 125
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitLeft*2000 ,125.0 , 3);
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitRight*2000 ,125.0 , 3);
    //Max Percentage out of PID should be 0.25*100
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorLeft*100 ,25 , 3);
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorRight*100 ,25 , 3);

    //Set enginepower right to 50% and left to 80%
    defineEnginePower(50,80);

    //Fifty percent forward should be a pwm value of 125
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitLeft*2000 ,110.0 , 3);
    munit_assert_double_equal(enginePowerLimit.enginePowerLimitRight*2000 ,125.0 , 3);
    //Max Percentage out of PID should be 0.25*100
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorLeft*100 ,40 , 3);
    munit_assert_double_equal(enginePowerLimit.engineDivideFactorRight*100 ,25 , 3);

	return MUNIT_OK;
}

static MunitResult percentageToPWM_Test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;
    int testValue;

    //Zero value to go straight
    testValue = percentageToPWM(0);
    munit_assert_int(testValue , ==, 150);

    //Positive max value to fully turn right
    testValue = percentageToPWM(100);
    munit_assert_int(testValue , ==, 200);

    //Negative max value to fully turn left
    testValue = percentageToPWM(-100);
    munit_assert_int(testValue , ==, 100);

    //Negative value to turn left
    testValue = percentageToPWM(-50);
    munit_assert_int(testValue , ==, 125);

    //Positive value to turn right
    testValue = percentageToPWM(50);
    munit_assert_int(testValue , ==, 175);

	return MUNIT_OK;
}
//static MunitResult addVertex_test(const MunitParameter params[], void* data)
//{
//    (void) params;
//    (void) data;
//
//    Graph *grapha = createGraph();
//    Vertex *a = addVertex(grapha, (char*)"a",(void*)"a");
//    Vertex *b = addVertex(grapha,(char*)"b",(void*)"b");
//    // BroJZ: verwijderd anders loopt programma vast
//    //Vertex *c = addVertex(grapha,(char*)"a",(void*)"c");
//    //Vertex *d = addVertex(grapha,(char*)"d",(void*)"d");
//    Graph *graphb = createGraph();
//    Vertex *e = addVertex(graphb,(char*)"a",(void*)"e");
//
//    munit_assert_not_null(a);
//    munit_assert_not_null(a->edges);
//    munit_assert_memory_equal(2,a->data,(void*)"a");
//    munit_assert_memory_equal(2,a->name,(char*)"a");
//
//    munit_assert_not_null(b);
//	munit_assert_not_null(b->edges);
//	munit_assert_memory_equal(2,b->data,(void*)"b");
//	munit_assert_memory_equal(2,b->name,(char*)"b");
//
////    munit_assert_not_null(d);
////	munit_assert_not_null(d->edges);
////	munit_assert_memory_equal(2,d->data,(void*)"d");
////	munit_assert_memory_equal(2,d->name,(char*)"d");
//
//    munit_assert_memory_not_equal(2,a->name, b->name);
////    munit_assert_null(c);
//
//    munit_assert_memory_equal(2,a->name, e->name);
//    munit_assert_not_null(e);
//	munit_assert_not_null(e->edges);
//	munit_assert_memory_equal(2,e->data,(void*)"e");
//	munit_assert_memory_equal(2,e->name,(char*)"a");
//
//    munit_assert_ptr_equal(a, grapha->vertices->head->data);
//    munit_assert_ptr_equal(b, grapha->vertices->head->next->data);
////    munit_assert_ptr_not_equal(c, grapha->vertices->head->next->next->data);
////    munit_assert_ptr_equal(d, grapha->vertices->head->next->next->data);
//
//    munit_assert_ptr_not_equal(a, b);
//    munit_assert_ptr_not_equal(a, e);
//
////    deleteGraph(&grapha);
//    deleteGraph(&graphb);
//
//    return MUNIT_OK;
//
//}
//
//static MunitResult findName_test(const MunitParameter params[], void *data)
//{
//    (void)params; // parameter not used (prevent warning)
//    (void)data;
//    char *charList[] = {"Tst1", "Tst2","Tst3"};
//    int dataint[3] = {1, 2, 3};
//    Vertex *vertList[3];
//
//    Graph *graph = createGraph();
//
//    for(int i = 0; i < 3; i++)
//    {
//        vertList[i] = addVertex(graph, charList[i], &dataint[i]);
//    }
//
//    char *ci = "Tst1";
//    Vertex *found = findName(graph, &equal_test, ci);
//    munit_assert_ptr_equal(vertList[0], found);
//
//    ci = "Tst2";
//    found = findName(graph, &equal_test, ci);
//    munit_assert_ptr_equal(vertList[1], found);
//
//    ci = "Tst3";
//    found = findName(graph, &equal_test, ci);
//    munit_assert_ptr_equal(vertList[2], found);
//
//    ci = "Tst4";
//    found = findName(graph, &equal_test, ci);
//    munit_assert_null(found);
//
//    deleteGraph(&graph);
//
//    return MUNIT_OK;
//}
//
//static MunitResult findData_test(const MunitParameter params[], void *data)
//{
//    (void)params; // parameter not used (prevent warning)
//    (void)data;
//    char *charList[] = {"Tst1", "Tst2","Tst3"};
//    int dataint[3] = {1, 2, 3};
//    Vertex *vertList[3];
//
//    Graph *graph = createGraph();
//
//    for(int i = 0; i < 3; i++)
//    {
//        vertList[i] = addVertex(graph, charList[i], &dataint[i]);
//    }
//
//    int ci = 1;
//    Vertex *found = findData(graph, &equal_test, &ci);
//    munit_assert_ptr_equal(vertList[0], found);
//
//    ci = 2;
//    found = findData(graph, &equal_test, &ci);
//    munit_assert_ptr_equal(vertList[1], found);
//
//    ci = 3;
//    found = findData(graph, &equal_test, &ci);
//    munit_assert_ptr_equal(vertList[2], found);
//
//    ci = 4;
//    found = findData(graph, &equal_test, &ci);
//    munit_assert_null(found);
//
//    deleteGraph(&graph);
//
//    return MUNIT_OK;
//}
//
//static MunitResult addEdgeDirectedUnweighted_test(const MunitParameter params[], void* data)
//{
//    (void) params;
//    (void) data;
//
//    Graph *grapha = createGraph();
//    Vertex *a = addVertex(grapha, (char*)"a",(void*)"a");
//    Vertex *b = addVertex(grapha,(char*)"b",(void*)"b");
//    Vertex *c = addVertex(grapha,(char*)"c",(void*)"c");
//    int compareWeight = 0;
//    addEdgeDirectedUnweighted(a, b);
//    addEdgeDirectedUnweighted(b, c);
//
//    Edge *aToB = a->edges->head->data;
//    Edge *bToA = b->edges->head->data;
//
//    munit_assert_ptr_not_null(aToB->destination);
//    munit_assert_ptr_equal(aToB->destination, b);
//    munit_assert_int(aToB->weight, ==, compareWeight);
//
//    munit_assert_ptr_not_null(bToA->destination);
//    munit_assert_ptr_equal(bToA->destination, c);
//    munit_assert_int(bToA->weight, ==, compareWeight);
//
//    deleteGraph(&grapha);
//
//	return MUNIT_OK;
//}
//
//static MunitResult addEdgeUndirectedUnweighted_test(const MunitParameter params[], void* data)
//{
//    (void) params;
//    (void) data;
//
//    Graph *grapha = createGraph();
//    Vertex *a = addVertex(grapha, (char*)"a",(void*)"a");
//    Vertex *b = addVertex(grapha,(char*)"b",(void*)"b");
//    int compareWeight = 0;
//    addEdgeUndirectedUnweighted(a, b);
//
//    Edge *aToB = a->edges->head->data;
//    Edge *bToA = b->edges->head->data;
//
//    munit_assert_ptr_not_null(aToB->destination);
//    munit_assert_ptr_not_null(bToA->destination);
//    munit_assert_ptr_equal(aToB->destination, b);
//    munit_assert_ptr_equal(bToA->destination, a);
//    munit_assert_int(aToB->weight, ==, compareWeight);
//    munit_assert_int(aToB->weight, ==, compareWeight);
//    munit_assert_int(aToB->weight, ==, bToA->weight);
//
//    deleteGraph(&grapha);
//
//    return MUNIT_OK;
//
//}
//
//static MunitResult addEdgeDirectedWeighted_test(const MunitParameter params[], void* data)
//{
//    (void) params;
//    (void) data;
//
//    Graph *grapha = createGraph();
//    Vertex *a = addVertex(grapha, (char*)"a",(void*)"a");
//    Vertex *b = addVertex(grapha,(char*)"b",(void*)"b");
//    Vertex *c = addVertex(grapha,(char*)"c",(void*)"c");
//    int compareWeight[2] = {5, 7};
//    addEdgeDirectedWeighted(a, b, 5);
//    addEdgeDirectedWeighted(b, c, 7);
//
//    Edge *aToB = a->edges->head->data;
//    Edge *bToA = b->edges->head->data;
//
//    munit_assert_ptr_not_null(aToB->destination);
//    munit_assert_ptr_equal(aToB->destination, b);
//    munit_assert_int(aToB->weight, ==, compareWeight[0]);
//
//    munit_assert_ptr_not_null(bToA->destination);
//    munit_assert_ptr_equal(bToA->destination, c);
//    munit_assert_int(bToA->weight, ==, compareWeight[1]);
//
//    deleteGraph(&grapha);
//
//    return MUNIT_OK;
//
//}
//
//static MunitResult addEdgeUndirectedWeighted_test(const MunitParameter params[], void* data)
//{
//    (void) params;
//    (void) data;
//
//    Graph *grapha = createGraph();
//    Vertex *a = addVertex(grapha, (char*)"a",(void*)"a");
//    Vertex *b = addVertex(grapha,(char*)"b",(void*)"b");
//    Vertex *c = addVertex(grapha,(char*)"c",(void*)"c");
//    int compareWeight[2] = {5, 7};
//    addEdgeUndirectedWeighted(a, b, 5);
//    addEdgeUndirectedWeighted(b, c, 7);
//
//    Edge *aToB = a->edges->head->data;
//    Edge *bToA = b->edges->head->data;
//    Edge *bToC = b->edges->head->next->data;
//    Edge *cToB = c->edges->head->data;
//
//    munit_assert_ptr_not_null(aToB->destination);
//    munit_assert_ptr_not_null(bToA->destination);
//    munit_assert_ptr_equal(aToB->destination, b);
//    munit_assert_ptr_equal(bToA->destination, a);
//    munit_assert_int(bToA->weight, ==, compareWeight[0]);
//    munit_assert_int(bToA->weight, ==, compareWeight[0]);
//    munit_assert_int(bToA->weight, ==, aToB->weight);
//
//    munit_assert_ptr_not_null(bToC->destination);
//    munit_assert_ptr_not_null(cToB->destination);
//    munit_assert_ptr_equal(bToC->destination, c);
//    munit_assert_ptr_equal(cToB->destination, b);
//    munit_assert_int(bToC->weight, ==, compareWeight[1]);
//    munit_assert_int(cToB->weight, ==, compareWeight[1]);
//    munit_assert_int(bToC->weight, ==, cToB->weight);
//
//    deleteGraph(&grapha);
//
//    return MUNIT_OK;
//}
//
//
//static MunitResult deleteGraph_empty(const MunitParameter params[], void *data)
//{
//    (void)params; // parameter not used (prevent warning)
//    (void)data;
//    Graph *graph = createGraph();
//    munit_assert_not_null(graph);
//
//    deleteGraph(&graph);
//
//    munit_assert_null(graph);
//
//    return MUNIT_OK;
//}
//
//
//MunitResult deleteGraph_filled(const MunitParameter params[], void *data)
//{
//    (void)params; // parameter not used (prevent warning)
//    (void)data;
//
//    Graph *graph = createGraph();
//    Vertex *a = addVertex(graph, (char*)"a",(void*)"a");
//    Vertex *b = addVertex(graph,(char*)"b",(void*)"b");
//    Vertex *c = addVertex(graph,(char*)"c",(void*)"c");
//    Vertex *d = addVertex(graph,(char*)"d",(void*)"d");
//
//    addEdgeUndirectedWeighted(b, d, 3);
//    addEdgeDirectedWeighted(c, a, 5);
//    addEdgeUndirectedUnweighted(c, d);
//    addEdgeDirectedUnweighted(a, b);
//
//    deleteGraph(&graph);
//
//    munit_assert_null(graph);
//
//    return MUNIT_OK;
//}
//
//static MunitResult deleteVertex_test(const MunitParameter params[], void* data)
//{
//
//    (void) params;
//    (void) data;
//
//    Graph *grapha = createGraph();
//    Vertex *a = addVertex(grapha, (char*)"a",(void*)"a");
//    Vertex *b = addVertex(grapha,(char*)"b",(void*)"b");
//    Vertex *c = addVertex(grapha,(char*)"c",(void*)"c");
//    Vertex *d = addVertex(grapha,(char*)"d",(void*)"d");
//
//    addEdgeDirectedUnweighted(d, b);
//    addEdgeDirectedUnweighted(c, b);
//    addEdgeDirectedUnweighted(d, c);
//    addEdgeUndirectedUnweighted(b, a);
//    Edge *dToc = d->edges->head->next->data;
//    Vertex *deletedVertex = grapha->vertices->head->next->data;
//    munit_assert_ptr_equal(grapha->vertices->head->next->data,b);
//
//    deleteVertex(grapha, b);
//
//    munit_assert_null(a->edges->head);
//    munit_assert_null(c->edges->head);
//    munit_assert_not_null(d->edges->head);
//    munit_assert_ptr_equal(dToc, d->edges->head->data);
//
//    munit_assert_ptr_not_equal(grapha->vertices->head->next->data,b);
//    munit_assert_ptr_null(findName(grapha, &equal_test, deletedVertex->name));
//
//    return MUNIT_OK;
//}

static MunitTest test_suite_tests[] = {
	{"/SetTo360", setTo360_Test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/determineError", determineError_Test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/degreeToPercentage", degreeToPercentage_Test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/percentageToPWM", percentageToPWM_Test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/defineEnginePower", defineEnginePower_Test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
//	{"/addEdgeUndirectedUnweighted", addEdgeUndirectedUnweighted_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
//	{"/addEdgeDirectedWeighted", addEdgeDirectedWeighted_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
//	{"/addEdgeUndirectedWeighted", addEdgeUndirectedWeighted_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
//	{"/deleteVertex", deleteVertex_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
//	{"/deleteGraph_empty", deleteGraph_empty, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
//	{"/deleteGraph_filled", deleteGraph_filled, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
    // Always add this one to tell runner testing is over!
    { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

// Main runner that picks which suites we should run
static const MunitSuite test_suite = {
    (char*) "test PID",
    test_suite_tests,
    NULL,
    1,
    MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char *argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
    return munit_suite_main(&test_suite, NULL, argc, argv);
}
