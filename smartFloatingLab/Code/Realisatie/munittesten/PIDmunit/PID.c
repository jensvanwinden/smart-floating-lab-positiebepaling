/*
 * Float_main.c
 *
 *  Created on: 4 dec. 2019
 *      Author: Chiel
 */

#include <stdio.h>
#include <math.h>
#include "stdint.h"
#include "PID.h"

#define lastTime 0.100
#define newTime 0.200
#define setpointTest 200
#define orientatieTest 200
#define maxITerm 150.0
#define minITerm -150.0
#define maxDTerm 25.0
#define minDTerm -25.0
#define maxPTerm 500
#define minPTerm -500
#define invertSteering 0
/*
 *
 */
float Kp = 2.0; //4
float KiSet = 1.0;
float Ki = 0.005; //0.02
float Kd = 2.5;
float ITerm = 0;
#define PWMPeriod 2000.0 //Chosen period

float IError = 0;
float lastError = -40;
//int lastTime = 0;

//int main(void)
//{
////	int b = 0;
////	int c = 0;
////	int orient = 100;
////    int i ;
////    for(i = 360; i >= -360; i -=10)
////    {
////        c = determineError(i,orient);
////        //printf("Orientatie = %d errorhoek = %d errorpercentage = %.3f%% PWMPeriod = %f  PWMTime = %d \n"
////        //		,orient, c, degreeToPercentage(c), PWMPeriod, percentageToPWM(degreeToPercentage(c)));
////    }
//	char test[] = {"50.0"};
//    calculatePID(100.0, test);
//
//    return 0;
//}

int setTo360(int degree)
{
	if(degree < 0)
	{
		degree = degree + 360;
	}

	return degree;
}

float degreeToPercentage(int degree)
{
	float degreeF = (float)degree;
	float percentage = degreeF * (100.0 / 180.0);
	return percentage;
}

int determineError(int setpointAngle, int orientationAngle)
{
	int errorAngle = 0;
	int angle1	= 0;
	int angle2 = 0;
	errorAngle = setTo360(setpointAngle) - setTo360(orientationAngle);

	if(errorAngle < -180 || errorAngle > 180)
	{
		angle1 = errorAngle + 360;
		angle2 = errorAngle - 360;

		if(pow(angle1,2) > pow(angle2,2))
		{
			return angle2;
		}
		else
		{
			return angle1;
		}
	}
	else
	{
		return errorAngle;
	}
}


//Wordt een void in cube printen voor debuggen
int percentageToPWM(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;

	if(PIDPercentage > 100)
	{
		PIDPercentage = 100;
	}
	if(PIDPercentage < -100)
	{
		PIDPercentage =-100;
	}

	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	if(PIDPercentage < 0) //links sturen
	{
		//TIM5->CCR2= PWMTimeF; //right ESC
	}

	if(PIDPercentage > 0) //rechts sturen
	{
		//TIM5->CCR1= 500; //left ESC
	}
	int PWMTimeI = (int)PWMTimeF;
	return PWMTimeI;
}


void calculatePID()
{
	static float errorPercentage = 0;
	static float ITerm = 0;
	static float totalGain = 0;

	errorPercentage = degreeToPercentage(determineError(setpointTest, orientatieTest));
	float sampleTimeF = newTime - lastTime;

	float PTerm =  errorPercentage;
	float DTerm = lastError - errorPercentage / sampleTimeF;
	ITerm += 	errorPercentage * sampleTimeF;

	lastError = errorPercentage;

	totalGain = Kp*PTerm + Kd*DTerm + Ki*ITerm;

}

void defineEnginePower(int8_t percentageRight, int8_t percentageLeft)
{
	enginePowerLimit.enginePowerLimitRight = (percentageRight * -0.00025) + 0.075;
	enginePowerLimit.engineDivideFactorRight = (150-(enginePowerLimit.enginePowerLimitRight*PWMPeriod))/100;

	enginePowerLimit.enginePowerLimitLeft = (percentageLeft * -0.00025) + 0.075;
	enginePowerLimit.engineDivideFactorLeft = (150-(enginePowerLimit.enginePowerLimitLeft*PWMPeriod))/100;
}
