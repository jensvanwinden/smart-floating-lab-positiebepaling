PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
PULSE_W3010
$EndINDEX
$MODULE PULSE_W3010
Po 0 0 0 15 00000000 00000000 ~~
Li PULSE_W3010
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -8.26 -4.935 1 1 0 0.05 N V 21 "PULSE_W3010"
T1 1.025 -4.935 1 1 0 0.05 N V 21 "VAL**"
DS -10.8 1.6 -10.8 2 0 24
DS -10.8 2 10.8 2 0 24
DS 10.8 2 10.8 1.6 0 24
DS -10.8 -1.6 -10.8 -4.25 0 24
DS -10.8 -4.25 10.8 -4.25 0 24
DS 10.8 -4.25 10.8 -1.6 0 24
DS -10.8 1.6 -10.8 2 0 24
DS -10.8 2 10.8 2 0 24
DS 10.8 2 10.8 1.6 0 24
DS -10.8 -1.6 -10.8 -4.25 0 24
DS -10.8 -4.25 10.8 -4.25 0 24
DS 10.8 -4.25 10.8 -1.6 0 24
DS -10.8 1.6 -10.8 2 0 24
DS -10.8 2 10.8 2 0 24
DS 10.8 2 10.8 1.6 0 24
DS -10.8 -1.6 -10.8 -4.25 0 24
DS -10.8 -4.25 10.8 -4.25 0 24
DS 10.8 -4.25 10.8 -1.6 0 24
DP 0 0 0 0 4 0 24
Dl -9.1 -4.25
Dl 9.1 -4.25
Dl 9.1 1.6
Dl -9.1 1.6
DP 0 0 0 0 4 0 24
Dl -10.8 1.6
Dl 10.8 1.6
Dl 10.8 2
Dl -10.8 2
DP 0 0 0 0 4 0 24
Dl 9.1 -0.15
Dl 10.8 -0.15
Dl 10.8 0.15
Dl 9.1 0.15
DP 0 0 0 0 4 0 24
Dl 9.1 -4.25
Dl 10.8 -4.25
Dl 10.8 -1.6
Dl 9.1 -1.6
DP 0 0 0 0 4 0 24
Dl -10.8 -4.25
Dl -9.1 -4.25
Dl -9.1 -1.6
Dl -10.8 -1.6
DP 0 0 0 0 4 0 24
Dl -10.8 1.6
Dl 10.8 1.6
Dl 10.8 2
Dl -10.8 2
DP 0 0 0 0 4 0 24
Dl -9.1 -4.25
Dl 9.1 -4.25
Dl 9.1 1.6
Dl -9.1 1.6
DP 0 0 0 0 4 0 24
Dl -10.8 -4.25
Dl -9.1 -4.25
Dl -9.1 -1.6
Dl -10.8 -1.6
DP 0 0 0 0 4 0 24
Dl 9.1 -0.15
Dl 10.8 -0.15
Dl 10.8 0.15
Dl 9.1 0.15
DP 0 0 0 0 4 0 24
Dl 9.1 -4.25
Dl 10.8 -4.25
Dl 10.8 -1.6
Dl 9.1 -1.6
DP 0 0 0 0 4 0 24
Dl -10.8 1.6
Dl 10.8 1.6
Dl 10.8 2
Dl -10.8 2
DP 0 0 0 0 4 0 24
Dl 9.1 -0.15
Dl 10.8 -0.15
Dl 10.8 0.15
Dl 9.1 0.15
DP 0 0 0 0 4 0 24
Dl 9.1 -4.25
Dl 10.8 -4.25
Dl 10.8 -1.6
Dl 9.1 -1.6
DP 0 0 0 0 4 0 24
Dl -10.8 -4.25
Dl -9.1 -4.25
Dl -9.1 -1.6
Dl -10.8 -1.6
DP 0 0 0 0 4 0 24
Dl -9.1 -4.25
Dl 9.1 -4.25
Dl 9.1 1.6
Dl -9.1 1.6
$PAD
Sh "3" R 1.7 3.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.95 0
$EndPAD
$PAD
Sh "1" R 1.7 1.45 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.95 -0.875
$EndPAD
$PAD
Sh "2" R 1.7 1.45 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.95 0.875
$EndPAD
$EndMODULE PULSE_W3010
