/*
 * rmc.c
 *
 *  Created on: 22 nov. 2019
 *      Author: Vince
 */

#include "rmc.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * Deze functie zorgt ervoor dat alle benodigde data wordt geparsed en opgeslagen.
 * data: Hierin staat de data die geparsed moet worden.
 * Het struct wat alle data bevat wordt na het parsen gereturned.
 */
RMC parseRMC(char data[])
{
	RMC new = {{0}, {0}, {0}, {0}, {0}, {0}, 0, 0, 0, 0};
	int offset = 0;
	offset = searchData(data,'$',offset);
	offset = searchData(data,',',offset);
	offset = offset + copyData(data+offset,new.time,',');
	offset = offset + copyData(data+offset,new.valid,',');

	if(*(new.valid) == 'A')	{

		offset = offset + copyData(data+offset,new.lat,',');
		offset = searchData(data,',',offset);
		offset = offset + copyData(data+offset,new.lon,',');
		offset = searchData(data,',',offset);
		offset = searchData(data,',',offset);
		offset = offset + copyData(data+offset,new.cog,',');
		offset = offset + copyData(data+offset,new.date,',');
	}
	else
	{
		return new;
	}

	new.latConv = atoi(new.lat)/100 + atof(new.lat+2) /60;
	new.lonConv = atoi(new.lon)/100 + atof(new.lon+3) /60;

	return new;
}

/*
 * Deze functie zorgt ervoor dat er wordt gezocht naar een bepaald karakter.
 * Wanneer het gevonden is zal de functie stoppen. Het eerste karakter wat overeenkomt met de zoekterm wordt gevonden.
 * data: Dit is de data waarin gezocht wordt.
 * until: Dit is het karakter waar naar gezocht wordt.
 * offset: Via deze variabele wordt een offset meegegeven vanaf waar er naar een
 * specifiek karakter wordt gezocht.
 * Deze functie returned de offset+1 van het gevonden karakter.
 */
int searchData(char data[], char until, int offset)
{
	int i = offset;
	while(*(data+i) != until)
	{
		i++;
	}
	return i+1;
}

/*
 * Deze functie kopieert de karakters van het meegegeven begin tot het meegegeven karakter wanneer het moet stoppen.
 * data: Dit is de data waarvan (een gedeelte) gekopieert wordt met de bijbehorende offset.
 * dest: Dit is de variabele waar de kopie in opgeslagen wordt.
 * until: Dit is het karakter waar het kopieeren stopt als het wordt tegengekomen. *
 * Deze functie returned de offset+1 van het gevonden karakter.
 */
int copyData(char data[], char dest[], char until)
{
	int i = 0;
	while(*(data+i) != until)
	{
		*(dest+i) = *(data+i);
		i++;
	}
	*(dest+i) = 0;
	return i+1;
}

/*
 * Deze functie berekent de bearing a.d.h.v. 2 locaties.
 * lat1: De latitude bijbehorend aan locatie 1.
 * lon1: De longitude bijbehorend aan locatie 1.
 * lat2: De latitude bijbehorend aan locatie 2.
 * lon2: De longitude bijbehorend aan locatie 2.
 * Deze functie returned de bearing.
 */
float getBearing(float lat1, float lon1, float lat2, float lon2)
{
	float x = 0, y = 0 , b = 0;

	if(lat1 == lat2 && lon1 == lon2)
	{
		return -1;
	}

	x = cos(lat2*PI_180)*sin((lon2-lon1)*PI_180);
	y = cos(lat1*PI_180)*sin(lat2*PI_180) - sin(lat1*PI_180) * cos(lat2*PI_180) * cos((lon2-lon1)*PI_180);

	b = atan2(x,y)*_180_PI;
	return b;
}

/*
 * Deze functie berekent de distance tussen 2 locaties m.b.v. de haversine formule.
 * lat1: De latitude bijbehorend aan locatie 1.
 * lon1: De longitude bijbehorend aan locatie 1.
 * lat2: De latitude bijbehorend aan locatie 2.
 * lon2: De longitude bijbehorend aan locatie 2.
 * Deze functie returned de afstand tussen de 2 locaties.
 */
float getDistance(float lat1, float lon1, float lat2, float lon2)
{
	float dlat = 0, dlon = 0, hav = 0;

	dlat = (lat2-lat1)*PI_180;
	dlon = (lon2-lon1)*PI_180;
	hav = pow(sin(dlat/2),2) + pow(sin(dlon/2),2) * cos(lat2*PI_180) * cos(lat1*PI_180);
	return 2* asin(sqrt(hav)) *	6371.0 * 1000.0;
}
