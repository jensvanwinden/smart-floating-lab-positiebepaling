/*
 * rmcTest.c
 *
 *  Created on: 22 nov. 2019
 *      Author: Vince
 */




#include "rmc.h"
#include "munit/munit.h"


static MunitResult search_test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;
    char test[] = "$GNRMC,101359.00,A,5201.05555,N,00425.31838,E,2.116,60.21,221119,,,D*4F";
    int offset = 0;

    offset = searchData(test,',',offset);
    munit_assert_char(*(test+offset), ==, '1');
    offset = searchData(test,',',offset);
    munit_assert_char(*(test+offset), ==, 'A');
    offset = searchData(test,',',offset);
    offset = searchData(test,',',offset);
    munit_assert_char(*(test+offset), ==, 'N');
	return MUNIT_OK;
}

static MunitResult copy_test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    char test[] = "$GNRMC,101359.00,A,5201.05555,N,00425.31838,E,2.116,60.21,221119,,,D*4F";
    int offset = 0;
    RMC copy = {{0}, {0}, {0}, {0}, {0}, {0}, 0, 0, 0, 0};

    offset = searchData(test,',',offset);
    offset = offset + copyData(test+offset,copy.time,',');
    munit_assert_string_equal(copy.time, "101359.00");
    offset = searchData(test,',',offset);
    offset = offset + copyData(test+offset,copy.lat,',');
    munit_assert_string_equal(copy.lat, "5201.05555");
    offset = searchData(test,',',offset);
	offset = offset + copyData(test+offset,copy.lon,',');
	munit_assert_string_equal(copy.lon, "00425.31838");
	offset = searchData(test,',',offset);
	offset = searchData(test,',',offset);
	offset = offset + copyData(test+offset,copy.cog,',');
	munit_assert_string_equal(copy.cog, "60.21");
	offset = offset + copyData(test+offset,copy.date,',');
	munit_assert_string_equal(copy.date, "221119");

	return MUNIT_OK;
}

static MunitResult parse_test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    char string1[] = "$GNRMC,101359.00,A,5201.05555,N,00425.31838,E,2.116,60.21,221119,,,D*4F";
    char string2[] = "$GNRMC,101434.00,V,,,,,,,221119,,,N*68";
    char string3[] = "$GNRMC,101351.00,A,5154.59023,N,00427.74974,E,0.028,,221119,,,D*7E";
    char string4[] = "1.05555,N,00425.31838,E,2.116,60.21,221119,,,D*4F$GNRMC,101351.00,A,5154.59023,N,00427.74974,E,0.028,,221119,,,D*7E";

    RMC test0 = parseRMC(string1);
    munit_assert_string_equal(test0.time, "101359.00");
    munit_assert_string_equal(test0.lat, "5201.05555");
    munit_assert_string_equal(test0.lon, "00425.31838");
    munit_assert_string_equal(test0.cog, "60.21");
    munit_assert_string_equal(test0.date, "221119");

    RMC test1 = parseRMC(string2);
    munit_assert_string_equal(test1.time, "101434.00");
    munit_assert_string_equal(test1.lat, "");
    munit_assert_string_equal(test1.lon, "");
    munit_assert_string_equal(test1.cog, "");
    munit_assert_string_equal(test1.date, "");

    RMC test2 = parseRMC(string3);
    munit_assert_string_equal(test2.time, "101351.00");
    munit_assert_string_equal(test2.lat, "5154.59023");
    munit_assert_string_equal(test2.lon, "00427.74974");
    munit_assert_string_equal(test2.cog, "");
    munit_assert_string_equal(test2.date, "221119");

    RMC test3 = parseRMC(string4);
    munit_assert_string_equal(test3.time, "101351.00");
    munit_assert_string_equal(test3.lat, "5154.59023");
    munit_assert_string_equal(test3.lon, "00427.74974");
    munit_assert_string_equal(test3.cog, "");
    munit_assert_string_equal(test3.date, "221119");

	return MUNIT_OK;
}

static MunitResult bearing_test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    char string1[] = "$GNRMC,101359.00,A,5201.05555,N,00425.31838,E,2.116,60.21,221119,,,D*4F";
    RMC test = parseRMC(string1);
    float a = getBearing(test.latConv,test.lonConv,51.909835800,4.462495800);
    munit_assert_float(a, ==, 166.9383124);

	return MUNIT_OK;
}

static MunitResult distance_test(const MunitParameter params[], void* data)
{
    (void) params;
    (void) data;

    char string1[] = "$GNRMC,101359.00,A,5201.05555,N,00425.31838,E,2.116,60.21,221119,,,D*4F";
    RMC test = parseRMC(string1);
    float a = getDistance(test.latConv,test.lonConv,51.909835800,4.462495800);
    munit_assert_float(a, ==, 12299.543);

	return MUNIT_OK;
}



static MunitTest test_suite_tests[] = {
	{"/search", search_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/copy", copy_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/parse", parse_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/bearing", bearing_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/distance", distance_test, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
    // Always add this one to tell runner testing is over!
    { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

// Main runner that picks which suites we should run
static const MunitSuite test_suite = {
    (char*) "test RMC",
    test_suite_tests,
    NULL,
    1,
    MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char *argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
    return munit_suite_main(&test_suite, NULL, argc, argv);
}
