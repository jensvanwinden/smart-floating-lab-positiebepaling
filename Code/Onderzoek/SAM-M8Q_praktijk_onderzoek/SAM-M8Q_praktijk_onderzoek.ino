String ID, tijd, lat, lon;

#include <NMEAGPS.h>
#include <GPSport.h>

NMEAGPS  gps; // This parses the GPS characters
gps_fix  fix; // This holds on to the latest values

#ifndef NMEAGPS_PARSE_RMC
#error You must define NMEAGPS_PARSE_GSV in NMEAGPS_cfg.h!
#endif

void setup()
{
  DEBUG_PORT.begin(9600);
  while (!Serial);
  DEBUG_PORT.print( F("SAM M8Q connected and ready for location plot\n") );
  gpsPort.begin(9600);

} // setup

void loop()
{
  while (gps.available( gpsPort )) 
  {
    fix = gps.read();
    
    showTalkerID();
    getLocation();    
  }
  
} // loop

void  getLocation()
{
  if (fix.valid.location) {

      String a = String(fix.dateTime.hours);
      String b = String(fix.dateTime.minutes);
      String c = String(fix.dateTime.seconds);
      tijd = a + b + c;
      //Serial.print(tijd);
      //DEBUG_PORT.print( ',' );
      lat = fix.latitudeL();
      lon = fix.longitudeL();
      //Serial.print(lat);
      //DEBUG_PORT.print( ',' );
      
      //Serial.print(lon);
      //DEBUG_PORT.print( ',' );
      
//      DEBUG_PORT.print( fix.dateTime.hours );
//      DEBUG_PORT.print( fix.dateTime.minutes );
//      DEBUG_PORT.print( fix.dateTime.seconds );
//      DEBUG_PORT.print( ',' );   
//      DEBUG_PORT.print( fix.latitude(), 6 );
//      DEBUG_PORT.print( ',' );
//      DEBUG_PORT.println( fix.longitude(), 6 );
    }
    
    else 
    {
      DEBUG_PORT.println( ' ' );
    }
}

void showTalkerID()
{
  int type = gps.nmeaMessage;
  String messageType = typeConvert(type);
//  DEBUG_PORT.print( '$' );
//  DEBUG_PORT.print( gps.talker_id );
//  DEBUG_PORT.print( messageType );
//  DEBUG_PORT.print( ',' );
  String talkerID = gps.talker_id;
  ID = "$" + talkerID + messageType;
 // DEBUG_PORT.print( ID );

}

String typeConvert (int type)
{
   switch (type) 
   {
    case 1:
      return "GGA";
      break;
    case 2:
      return "GLL";
      break;
    case 3:
      return "GSA";
      break;
    case 4:
      return "GST";
      break;
    case 5:
      return "GSV";
      break;
    case 6:
      return "RMC";
      break;
    case 7:
      return "VTG";
      break;
    case 8:
      return "ZDA";
      break;
  }
}
