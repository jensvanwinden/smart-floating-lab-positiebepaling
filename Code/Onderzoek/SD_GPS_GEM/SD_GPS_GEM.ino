String ID, tijd, lat, lon;
int check = 1, d = 0;
int32_t dLat, dLon;
float avgDistError;
#include <NMEAGPS.h>
#include <GPSport.h>
#include <SD.h> //Load SD library
int chipSelect = 4; //chip select pin for the MicroSD Card Adapter
File file; // file object that is used to read and write data

NMEAGPS  gps; // This parses the GPS characters
gps_fix  fix; // This holds on to the latest values


using namespace NeoGPS;
static gps_fix    first;            // good GPS data
static clock_t    firstSecs;        // cached dateTime in seconds since EPOCH
static Location_t avgLoc;           // gradually-calculated average location
static uint16_t   count;            // number of samples
static int32_t    sumDLat, sumDLon; // accumulated deltas
static bool       doneAccumulating; // accumulation completed

#ifndef NMEAGPS_PARSE_RMC
#error You must define NMEAGPS_PARSE_RMC in NMEAGPS_cfg.h!
#endif

void setup()
{
  Serial.begin(9600); // start serial connection to print out debug messages and data
  DEBUG_PORT.begin(9600);
  while (!DEBUG_PORT);

  DEBUG_PORT.println("count,avg lat,avg lon,dlat,dlon,distance(cm)");
  DEBUG_PORT.flush();

  gpsPort.begin( 9600 );
  pinMode(chipSelect, OUTPUT); // chip select pin must be set to OUTPUT mode
  if (!SD.begin(chipSelect))
  { // Initialize SD card
    Serial.println("Could not initialize SD card."); // if return value is false, something went wrong.
  }
}

void loop()
{
  while (gps.available( gpsPort ))
  {
    fix = gps.read();
    doSomeWork();
    showTalkerID();
    getLocation();
    file = SD.open("datalog.csv", FILE_WRITE);
    if (file) {

      file.print(ID); // write number to file
      file.print(',');
      file.print(tijd);
      file.print(',');
      file.print(String(avgLoc.lat()));
      file.print(',');
      file.print(avgLoc.lon());
      file.print(',');
      file.print(dLat);
      file.print(',');
      file.print(dLon);
      file.print(',');
      file.print(avgDistError);
      file.print(',');
      file.println(check);
      file.close(); // close file
      Serial.println(", writing OK");
    }
    else
    {
      Serial.println("Could not open file (writing).");
    }
  }
}

void  getLocation()
{
  if (fix.valid.location)
  {
    String a, b, c;

    if (d == fix.dateTime.seconds)
    {
      check = 0;
      Serial.print("Old NMEA fix, ");
    }
    else
    {
      check = 1;
      Serial.print("fix OK, ");
    }

    if (fix.dateTime.hours < 10)
    {
      a = "0" + String(fix.dateTime.hours);
    }
    else
    {
      a = String(fix.dateTime.hours);
    }

    if (fix.dateTime.minutes < 10)
    {
      b = "0" + String(fix.dateTime.minutes);
    }
    else
    {
      b = String(fix.dateTime.minutes);
    }

    if (fix.dateTime.seconds < 10)
    {
      c = "0" + String(fix.dateTime.seconds);
    }
    else
    {
      c = String(fix.dateTime.seconds);
    }

    d = fix.dateTime.seconds;

    tijd = a + b + c;
    Serial.print(tijd);
   
  }

  else
  {
    DEBUG_PORT.println( ' ' );
  }
}

void showTalkerID()
{
  int type = gps.nmeaMessage;
  String messageType = typeConvert(type);
  //  DEBUG_PORT.print( '$' );
  //  DEBUG_PORT.print( gps.talker_id );
  //  DEBUG_PORT.print( messageType );
  //  DEBUG_PORT.print( ',' );
  String talkerID = gps.talker_id;
  ID = "$" + talkerID + messageType;
  // DEBUG_PORT.print( ID );
}

String typeConvert (int type)
{
  switch (type)
  {
    case 1:
      return "GGA";
      break;
    case 2:
      return "GLL";
      break;
    case 3:
      return "GSA";
      break;
    case 4:
      return "GST";
      break;
    case 5:
      return "GSV";
      break;
    case 6:
      return "RMC";
      break;
    case 7:
      return "VTG";
      break;
    case 8:
      return "ZDA";
      break;
  }
}

static void doSomeWork()
{
  static bool warned = false; // that we're waiting for a valid location

  if (fix.valid.location && fix.valid.date && fix.valid.time) {

    if (count == 0) {
    
      // Just save the first good fix
      first = fix;
      firstSecs = (clock_t) first.dateTime;
      count = 1;

    } else {

      // After the first fix, accumulate locations until we have
      //   a good average.  Then display the offset from the average.

      if (warned) {
        // We were waiting for the fix to be re-acquired.
        warned = false;
        DEBUG_PORT.println();
      }

      DEBUG_PORT.print( count );

      if (!doneAccumulating) {

        // Enough time?
        if (((clock_t)fix.dateTime - firstSecs) > 2 * SECONDS_PER_HOUR)
          doneAccumulating = true;
      }

      

      if (!doneAccumulating) {

        // Use deltas from the first location
        dLat = fix.location.lat() - first.location.lat();
        sumDLat += dLat;
        int32_t avgDLat = sumDLat / count;

        dLon = fix.location.lon() - first.location.lon();
        sumDLon += dLon;
        int32_t avgDLon = sumDLon / count;

        //  Then calculated the average location as the first location
        //     plus the averaged deltas.
        avgLoc.lat( first.location.lat() + avgDLat );
        avgLoc.lon( first.location.lon() + avgDLon );

        count++;
      }

      DEBUG_PORT.print( ',' );
      DEBUG_PORT.print( avgLoc.lat() );
      DEBUG_PORT.print( ',' );
      DEBUG_PORT.print( avgLoc.lon() );
      DEBUG_PORT.print( ',' );
      dLat = avgLoc.lat() - fix.location.lat();
      DEBUG_PORT.print( dLat );
      DEBUG_PORT.print( ',' );
      dLon = avgLoc.lon() - fix.location.lon();
      DEBUG_PORT.print( dLon );

      // Calculate the distance from the current fix to the average location
      avgDistError = avgLoc.DistanceKm( fix.location ) * 100000.0;
      DEBUG_PORT.print( ',' );
      DEBUG_PORT.print( avgDistError ); // cm
    }

  } else {
    if (!warned) {
      warned = true;
      DEBUG_PORT.print( F("Waiting for fix...") );
    } else {
      DEBUG_PORT.print( '.' );
    }
  }

} // doSomeWork
