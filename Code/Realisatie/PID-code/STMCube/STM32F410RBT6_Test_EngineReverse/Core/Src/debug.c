/*
 * debug.c
 *
 *  Created on: Dec 2, 2019
 *      Author: Vince
 */

#include "debug.h"
#include <stdlib.h>
#include <stdio.h>


/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 9600;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

void debugCharPrint(char sendMessage[])
{
	//char newline[2] = "\r\n";
	HAL_UART_Transmit(&huart6, (uint8_t*) sendMessage, strlen(sendMessage), HAL_MAX_DELAY);
	//HAL_UART_Transmit(&huart6, (uint8_t*) newline, 2, HAL_MAX_DELAY);
}

void debugIntPrint(int message)
{
	char buffer[20] = {0};
	itoa(message,buffer,10);
	debugCharPrint(buffer);
}

void debugPrintEnter(void)
{
	char newline[2] = "\r\n";
	HAL_UART_Transmit(&huart6, (uint8_t*) newline, 2, HAL_MAX_DELAY);
}

void debugFloatPrint(float message)
{
	char buffer[10] = {0};
	gcvt(message, 5, buffer);
	//sprintf(buffer, "%g", message);
	debugCharPrint(buffer);
}

