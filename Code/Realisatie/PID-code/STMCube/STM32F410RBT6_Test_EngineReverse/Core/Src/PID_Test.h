/*
 * PID_Test.h
 *
 *  Created on: 6 dec. 2019
 *      Author: ToBeEdited
 */

#ifndef SRC_PID_TEST_H_
#define SRC_PID_TEST_H_

#define leftEngine TIM5->CCR1
#define rightEngine TIM5->CCR2
#define servo TIM5->CCR3


enum engineDirection {forward, off, backward, rightTurn, leftTurn};
//enum engineType {leftEngine = TIM5->CCR1, rightEngine = TIM5->CCR2, servo = TIM5->CCR3};


int16_t percentageToPWM(float PIDPercentage);
float degreeToPercentage(int16_t degree);
int16_t setTo360(int16_t degree);
int16_t determineError(int16_t setpointAngle, int16_t orientationAngle);
void percentageToPWMvoid(float PIDPercentage);
void calculatePID();
void engineMode(uint8_t engineMode);
void initEngine();

#endif /* SRC_PID_TEST_H_ */
