/*
 * PID_Test.c
 *
 *  Created on: 6 dec. 2019
 *      Author: ToBeEdited
 */


#ifndef SRC_PID_TEST_C_
#define SRC_PID_TEST_C_

#include "main.h"
#include <stdio.h>
#include <math.h>
#include "PID_Test.h"
#include "debug.h"
#include "stdlib.h"

/*
 *
 Debug defines om totale pid te kunnen testen tussen comment-----------------------------
*/
//#define lastTime 0.100
//#define newTime 0.200
//#define setpointTest 200
//#define orientatieTest 200
/*
 *------------------------------------------------------------------------------------------
 */
float Kc = 0.1;
float KiSet = 1.0;
float Ki = 0.1;
float Kd = 0.1;
#define PWMPeriod 2000.0 //Chosen period
#define invertSteering 1
#define engineSteering 1

float lastError = 0;
int16_t setpointTest = 270;
int16_t orientatieTest = 80;


int16_t setTo360(int16_t degree)
{
	if(degree < 0)
	{
		degree = degree + 360;
	}

	return degree;
}

float degreeToPercentage(int16_t degree)
{
	float degreeF = (float)degree;
	float percentage = degreeF * (100.0 / 180.0);

	if(invertSteering == 1)
	{
		percentage = percentage*-1;
	}

	debugCharPrint(" degreegTo% = ");
	debugFloatPrint(percentage);
	debugPrintEnter();
	return percentage;
}

int16_t determineError(int16_t setpointAngle, int16_t orientationAngle)
{
	int16_t errorAngle = 0;
	int16_t angle1	= 0;
	int16_t angle2 = 0;
	errorAngle = setTo360(setpointAngle) - setTo360(orientationAngle);
	//errorAngle = setpointAngle - orientationAngle;

	debugCharPrint(" Setpoint = ");
	debugIntPrint(setTo360(setpointAngle));
	debugPrintEnter();
	debugCharPrint(" heading = ");
	debugIntPrint(setTo360(orientationAngle));
	debugPrintEnter();

	if(errorAngle < -180 || errorAngle > 180)
	{
		angle1 = errorAngle + 360;
		angle2 = errorAngle - 360;

		if(pow(angle1,2) > pow(angle2,2))
		{
			debugCharPrint(" Errorangle = ");
			debugFloatPrint(angle2);
			debugPrintEnter();
			return angle2;
		}
		else
		{
			debugCharPrint(" Errorangle = ");
			debugFloatPrint(angle1);
			debugPrintEnter();
			return angle1;
		}
	}
	else
	{
		debugCharPrint(" Errorangle = ");
		debugFloatPrint(errorAngle);
		debugPrintEnter();
		return errorAngle;
	}
}

void calculatePID(float bearing, char* cog)
{
	//Get Bearing functie en Cog functie

	static float newError = 0;
	static float ITerm = 0;
	float DTerm = 0;
	float PTerm = 0;
	static float totalGain = 0;



	int16_t setPoint = (int16_t)bearing;
    int16_t	orientatie = atoi(cog);


    newError = degreeToPercentage(determineError(setPoint, orientatie));  				// with bearing and cog
	//newError = degreeToPercentage(determineError(setpointTest, orientatieTest));
	extern float newTime;
	extern float lastTime;
//	uint16_t sampleTime = newTime - lastTime;
//	float sampleTimeF = (float)sampleTime;
	float sampleTimeF = newTime - lastTime;

	PTerm =  newError;
	DTerm = (lastError - newError) / sampleTimeF;
	ITerm += 	(KiSet*newError) * sampleTimeF;

	totalGain = Kc*PTerm + Kd*DTerm + Ki*ITerm;

//	if(((totalGain > 100) || (totalGain < 100)) && (((lastError < 0) && (newError < 0)) || ((lastError > 0) && (newError > 0))))
//	{
//		//if(((lastError < 0) && (errorPercentage < 0) || (lastError > 0) && (errorPercentage > 0)))
//		//{
//			KiSet = 0.0;
//		//}
//	}
//	else
//	{
//		KiSet = 1.0;
//	}

	if((totalGain > 100) && (lastError > 0) && (newError > 0))
	{
		KiSet = 0.0;
	}
	else if((totalGain < -100) && (lastError < 0) && (newError < 0))
	{
		KiSet = 0.0;
	}
	else
	{
		KiSet = 1.0;
	}

	//percentageToPWMvoid(totalGain);				comment for debugging

	/*DEBUG
	 * -------------------------------------------------------------------------------
	 */
	uint16_t test = percentageToPWM(totalGain);


	debugCharPrint(" Sampletime = ");
	debugFloatPrint(sampleTimeF);
	debugPrintEnter();
	debugCharPrint(" newTime = ");
	debugFloatPrint(newTime);
	debugPrintEnter();
	debugCharPrint(" lastTime = ");
	debugFloatPrint(lastTime);
	debugPrintEnter();
	debugCharPrint(" KiSet = ");
	debugFloatPrint(KiSet);
	debugPrintEnter();
	debugCharPrint(" newError = ");
	debugFloatPrint(newError);
	debugPrintEnter();
	debugCharPrint(" lastError = ");
	debugFloatPrint(lastError);
	debugPrintEnter();
	debugCharPrint(" PTerm = ");
	debugIntPrint(PTerm);
	debugPrintEnter();
	debugCharPrint(" DTerm = ");
	debugIntPrint(DTerm);
	debugPrintEnter();
	debugCharPrint(" ITerm = ");
	debugIntPrint(ITerm);
	debugPrintEnter();
	debugCharPrint(" totalPID% = ");
	debugFloatPrint(totalGain);
	debugPrintEnter();
	debugCharPrint(" % to pwm = ");
	debugIntPrint(test);
	debugPrintEnter();
	/*
	 * --------------------------------------------------------------------------------
	 */
	percentageToPWMvoid(totalGain);
	lastTime = newTime;
	lastError = newError;
}


void percentageToPWMvoid(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;
	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	if(engineSteering == 1)
	{
		if(PIDPercentage < 0) //links sturen
		{
			rightEngine = PWMPeriod*0.05;
			leftEngine = PWMPeriod*0.0625;
		}

		else if(PIDPercentage > 0) //rechts sturen
		{
			leftEngine = PWMPeriod*0.05;
			rightEngine = PWMPeriod*0.0625;
		}
	}

	uint16_t PWMTimeI = (uint16_t)PWMTimeF; 					//Debug

	servo = PWMTimeI; 										//Servo

}


int16_t percentageToPWM(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;

	if(PIDPercentage > 100)
	{
		PIDPercentage = 100;
	}
	if(PIDPercentage < -100)
	{
		PIDPercentage =-100;
	}

	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	int16_t PWMTimeI = (int16_t)PWMTimeF;
	return PWMTimeI;
}

void engineMode(uint8_t engineMode)
{
	switch(engineMode)
	{
		case forward:
			{
				rightEngine= PWMPeriod*0.05; //links
				leftEngine= PWMPeriod*0.05; //rechts
			}
		break;
		case off:
		{
				rightEngine= PWMPeriod*0.075;
				leftEngine= PWMPeriod*0.075;
		}
		break;
		case backward:
		{
				rightEngine= PWMPeriod*0.1;
				leftEngine= PWMPeriod*0.1;
		}
				break;
		case leftTurn:								//Debug case
		{
			rightEngine= PWMPeriod*0.075;
			leftEngine= PWMPeriod*0.1;
		}
		break;
		case rightTurn:								//Debug case
		{
			rightEngine= PWMPeriod*0.1;
			leftEngine= PWMPeriod*0.075;
		}
		break;
	}

//	if (engineMode == forward)
//		{
//			rightEngine= 100; //links
//			leftEngine= 100; //rechts
//		}
//	else if (engineMode == off)
//	{
//		rightEngine= 150;
//		leftEngine= 150;
//	}
//	else if(engineMode == backward)
//	{
//		rightEngine= 200;
//		leftEngine= 200;
//	}
}

void initEngine()
{
	rightEngine = 150; //links
	leftEngine = 150; //rechts
	servo = 150; //Servo
	HAL_Delay(2000);
}

#endif /* SRC_PID_TEST_C_ */
