/*
 * PID_Test.h
 *
 *  Created on: 6 dec. 2019
 *      Author: ToBeEdited
 */

#ifndef SRC_PID_TEST_H_
#define SRC_PID_TEST_H_

int16_t percentageToPWM(float PIDPercentage);
float degreeToPercentage(int16_t degree);
int16_t setTo360(int16_t degree);
int16_t determineError(int16_t setpointAngle, int16_t orientationAngle);

void percentageToPWMvoid(float PIDPercentage);
void calculatePID();

#endif /* SRC_PID_TEST_H_ */
