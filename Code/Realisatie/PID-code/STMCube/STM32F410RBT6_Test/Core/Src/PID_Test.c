/*
 * PID_Test.c
 *
 *  Created on: 6 dec. 2019
 *      Author: ToBeEdited
 */


#ifndef SRC_PID_TEST_C_
#define SRC_PID_TEST_C_

#include "main.h"
#include <stdio.h>
#include <math.h>
#include "PID_Test.h"
#include "debug.h"

/*
 *
 Debug defines om totale pid te kunnen testen tussen comment-----------------------------
*/
//#define lastTime 0.100
//#define newTime 0.200
//#define setpointTest 200
//#define orientatieTest 200
/*
 *------------------------------------------------------------------------------------------
 */
float Kc = 0.5;
float KiSet = 1.0;
float Ki = 0.5;
float Kd = 0.5;
#define PWMPeriod 2000.0 //Chosen period

float lastError = 0;
int16_t setpointTest = 270;
int16_t orientatieTest = 80;


int16_t setTo360(int16_t degree)
{
	if(degree < 0)
	{
		degree = degree + 360;
	}

	return degree;
}

float degreeToPercentage(int16_t degree)
{
	float degreeF = (float)degree;
	float percentage = degreeF * (100.0 / 180.0);
	debugCharPrint(" degreegTo% = ");
	debugFloatPrint(percentage);
	debugPrintEnter();
	return percentage;
}

int16_t determineError(int16_t setpointAngle, int16_t orientationAngle)
{
	int16_t errorAngle = 0;
	int16_t angle1	= 0;
	int16_t angle2 = 0;
	errorAngle = setTo360(setpointAngle) - setTo360(orientationAngle);
	//errorAngle = setpointAngle - orientationAngle;

	if(errorAngle < -180 || errorAngle > 180)
	{
		angle1 = errorAngle + 360;
		angle2 = errorAngle - 360;

		if(pow(angle1,2) > pow(angle2,2))
		{
			debugCharPrint(" Errorangle = ");
			debugFloatPrint(angle2);
			debugPrintEnter();
			return angle2;
		}
		else
		{
			debugCharPrint(" Errorangle = ");
			debugFloatPrint(angle1);
			debugPrintEnter();
			return angle1;
		}
	}
	else
	{
		debugCharPrint(" Errorangle = ");
		debugFloatPrint(errorAngle);
		debugPrintEnter();
		return errorAngle;
	}
}

void calculatePID()
{
	//Get Bearing functie en Cog functie

	static float newError = 0;
	static float ITerm = 0;
	float DTerm = 0;
	float PTerm = 0;
	static float totalGain = 0;

	//errorPercentage = degreeToPercentage(determineError(RMC., RMC.));  				// with bearing and cog
	newError = degreeToPercentage(determineError(setpointTest, orientatieTest));
	extern float newTime;
	extern float lastTime;
//	uint16_t sampleTime = newTime - lastTime;
//	float sampleTimeF = (float)sampleTime;
	float sampleTimeF = newTime - lastTime;

	PTerm =  newError;
	DTerm = (lastError - newError) / sampleTimeF;
	ITerm += 	(KiSet*newError) * sampleTimeF;

	totalGain = Kc*PTerm + Kd*DTerm + Ki*ITerm;

//	if(((totalGain > 100) || (totalGain < 100)) && (((lastError < 0) && (newError < 0)) || ((lastError > 0) && (newError > 0))))
//	{
//		//if(((lastError < 0) && (errorPercentage < 0) || (lastError > 0) && (errorPercentage > 0)))
//		//{
//			KiSet = 0.0;
//		//}
//	}
//	else
//	{
//		KiSet = 1.0;
//	}

	if((totalGain > 100) && (lastError > 0) && (newError > 0))
	{
		KiSet = 0.0;
	}
	else if((totalGain < -100) && (lastError < 0) && (newError < 0))
	{
		KiSet = 0.0;
	}
	else
	{
		KiSet = 1.0;
	}

	//percentageToPWMvoid(totalGain);				comment for debugging

	/*DEBUG
	 * -------------------------------------------------------------------------------
	 */
	uint16_t test = percentageToPWM(totalGain);
	debugCharPrint(" Setpoint = ");
	//debugFloatPrint(sampleTimeF);
	debugPrintEnter();
	debugCharPrint(" heading = ");
	//debugFloatPrint(sampleTimeF);
	debugPrintEnter();
	debugCharPrint(" Sampletime = ");
	debugFloatPrint(sampleTimeF);
	debugPrintEnter();
	debugCharPrint(" newTime = ");
	debugFloatPrint(newTime);
	debugPrintEnter();
	debugCharPrint(" lastTime = ");
	debugFloatPrint(lastTime);
	debugPrintEnter();
	debugCharPrint(" KiSet = ");
	debugFloatPrint(KiSet);
	debugPrintEnter();
	debugCharPrint(" newError = ");
	debugFloatPrint(newError);
	debugPrintEnter();
	debugCharPrint(" lastError = ");
	debugFloatPrint(lastError);
	debugPrintEnter();
	debugCharPrint(" PTerm = ");
	debugIntPrint(PTerm);
	debugPrintEnter();
	debugCharPrint(" DTerm = ");
	debugIntPrint(DTerm);
	debugPrintEnter();
	debugCharPrint(" ITerm = ");
	debugIntPrint(ITerm);
	debugPrintEnter();
	debugCharPrint(" totalPID% = ");
	debugIntPrint(totalGain);
	debugPrintEnter();
	debugCharPrint(" % to pwm = ");
	debugIntPrint(test);
	debugPrintEnter();
	debugPrintEnter();
	/*
	 * --------------------------------------------------------------------------------
	 */
	lastTime = newTime;
	lastError = newError;
}


void percentageToPWMvoid(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;
	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	if(PIDPercentage < 0) //links sturen
	{
		//	TIM5->CCR1= 500; //links
		//	TIM5->CCR2= 1250; //rechts
		//	TIM5->CCR3= 500; //servo
	}

	if(PIDPercentage > 0) //rechts sturen
	{
		//	TIM5->CCR1= 500; //links
		//	TIM5->CCR2= 1250; //rechts
		//	TIM5->CCR3= 500; //servo
	}


	uint16_t PWMTimeI = (uint16_t)PWMTimeF;
	TIM5->CCR3= PWMTimeI; 					//Servo

}


//Wordt een void in cube printen voor debuggen
int16_t percentageToPWM(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;

	if(PIDPercentage > 100)
	{
		PIDPercentage = 100;
	}
	if(PIDPercentage < -100)
	{
		PIDPercentage =-100;
	}

	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	if(PIDPercentage < 0) //links sturen
	{
		//TIM5->CCR2= PWMTimeF; //right ESC
	}

	if(PIDPercentage > 0) //rechts sturen
	{
		//TIM5->CCR1= 500; //left ESC
	}
//	TIM5->CCR1= 500; //links
//	TIM5->CCR2= 1250; //rechts
//	TIM5->CCR3= 500; //servo

	int16_t PWMTimeI = (int16_t)PWMTimeF;
	return PWMTimeI;
}

#endif /* SRC_PID_TEST_C_ */
