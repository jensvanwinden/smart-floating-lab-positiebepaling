/*
 * PID_init.h
 *
 *  Created on: Dec 12, 2019
 *      Author: jens
 */

#ifndef SRC_PID_INIT_H_
#define SRC_PID_INIT_H_

#include "main.h"

TIM_HandleTypeDef htim5;
void MX_TIM5_Init(void);
void PID_init(void);

#endif /* SRC_PID_INIT_H_ */
