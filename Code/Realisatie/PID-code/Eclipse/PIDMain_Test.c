/*
 * Float_main.c
 *
 *  Created on: 4 dec. 2019
 *      Author: ToBeEdited
 */

#include <stdio.h>
#include <math.h>

/*
 * Debug defines om totale pid te kunnen testen tussen comment
 */
#define lastTime 0.100
#define newTime 0.200
#define setpointTest 200
#define orientatieTest 200
/*
 *
 */
#define Kc 0.5
#define Ki 0.5
#define Kd 0.5
#define PWMPeriod 2000.0 //Chosen period

float IError = 0;
float lastError = -40;
//int lastTime = 0;

float degreeToPercentage(int degree);
int setTo360(int degree);
int determineError(int setpointAngle, int orientationAngle);
int percentageToPWM(float PIDPercentage);
//void calculatePIDPercentage(float errorPercentage)
void calculatePID();

int main(void)
{
	int b = 0;
	int c = 0;
	int orient = 100;
    int i ;
    for(i = 360; i >= -360; i -=10)
    {
        c = determineError(i,orient);
        //printf("Orientatie = %d errorhoek = %d errorpercentage = %.3f%% PWMPeriod = %f  PWMTime = %d \n"
        //		,orient, c, degreeToPercentage(c), PWMPeriod, percentageToPWM(degreeToPercentage(c)));
    }
    calculatePID();

    return 0;
}

int setTo360(int degree)
{
	if(degree < 0)
	{
		degree = degree + 360;
	}

	return degree;
}

float degreeToPercentage(int degree)
{
	float degreeF = (float)degree;
	float percentage = degreeF * (100.0 / 180.0);
	return percentage;
}

int determineError(int setpointAngle, int orientationAngle)
{
	int errorAngle = 0;
	int angle1	= 0;
	int angle2 = 0;
	errorAngle = setTo360(setpointAngle) - setTo360(orientationAngle);
	//errorAngle = setpointAngle - orientationAngle;


	if(errorAngle < -180 || errorAngle > 180)
	{
		angle1 = errorAngle + 360;
		angle2 = errorAngle - 360;

		if(pow(angle1,2) > pow(angle2,2))
		{
			return angle2;
		}
		else
		{
			return angle1;
		}
	}
	else
	{
		return errorAngle;
	}
}


//Wordt een void in cube printen voor debuggen
int percentageToPWM(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;

	if(PIDPercentage > 100)
	{
		PIDPercentage = 100;
	}
	if(PIDPercentage < -100)
	{
		PIDPercentage =-100;
	}

	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	if(PIDPercentage < 0) //links sturen
	{
		//TIM5->CCR2= PWMTimeF; //right ESC
	}

	if(PIDPercentage > 0) //rechts sturen
	{
		//TIM5->CCR1= 500; //left ESC
	}
//	TIM5->CCR1= 500; //links
//	TIM5->CCR2= 1250; //rechts
//	TIM5->CCR3= 500; //servo
	int PWMTimeI = (int)PWMTimeF;
	return PWMTimeI;
}

//void calculatePIDPercentage(float errorPercentage)
//{
//	static float ITerm;
//	int sampleTime = newTime - lastTime;
//	float sampleTimeF = (float) sampleTime;
//	float PTerm =  errorPercentage;
//	float DTerm = lastError - errorPercentage / sampleTimeF;
//	ITerm += 	errorPercentage * sampleTimeF;
//
//	lastError = errorPercentage;
//	lastTime = newTime;
//	float totalGain = Kc*PTerm + Kd*DTerm + Ki*ITerm;
//}

void calculatePID()
{
	//Get Bearing functie en Cog functie

	static float errorPercentage = 0;
	static float ITerm = 0;
	static float totalGain = 0;

	errorPercentage = degreeToPercentage(determineError(setpointTest, orientatieTest));
	float sampleTimeF = newTime - lastTime;

	//float sampleTimeF = (float) sampleTime;

	float PTerm =  errorPercentage;
	float DTerm = lastError - errorPercentage / sampleTimeF;
	ITerm += 	errorPercentage * sampleTimeF;

	lastError = errorPercentage;
	//lastTime = newTime;

	totalGain = Kc*PTerm + Kd*DTerm + Ki*ITerm;

	int test = percentageToPWM(totalGain);
	printf("pterm = %.3f dterm = %.3f iterm = %.3f sampletimeF = %.3f\n",
			PTerm, DTerm, ITerm, sampleTimeF);
	printf("totalGain = %f errorpercentage = %.3f  final output ptoPWM = %d\n",totalGain, errorPercentage, test);
}
