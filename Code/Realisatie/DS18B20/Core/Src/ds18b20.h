/*
 * ds18b20.h
 *
 *  Created on: Nov 29, 2019
 *      Author: Vince
 */

#ifndef SRC_DS18B20_H_
#define SRC_DS18B20_H_

#include <stdio.h>

void DS18B20_mode(int mode);
uint8_t temp_init(void);
void write1(void);
void write0(void);
uint8_t readbit(void);
void writebyte(uint8_t value);
uint8_t readbyte(void);
void convertT(void);
uint16_t readscratchpad(void);
void writescratchpad(void);
void gettemp(void);
uint16_t readtemp(void);

#endif /* SRC_DS18B20_H_ */
