/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/

#include "ds18b20.h"
#include "main.h"

#define OUT 1
#define IN	0


/* USER CODE BEGIN 4 */
void DS18B20_mode(int mode)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(mode == 0)
	{
		  GPIO_InitStruct.Pin = GPIO_PIN_9;
		  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		  GPIO_InitStruct.Pull = GPIO_NOPULL;
		  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	}
	else
	{
		  /*Configure GPIO pin : PB9 */
		  GPIO_InitStruct.Pin = GPIO_PIN_9;
		  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		  GPIO_InitStruct.Pull = GPIO_NOPULL;
		  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	}
}

uint8_t temp_init(void)
{

	DS18B20_mode(OUT);			//PC5 output
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);  	//PC5 0V
	TIM6->CNT = 0;
	while(TIM6->CNT < 1000);	//500us wachten
	DS18B20_mode(IN);
	TIM6->CNT = 0;
	while(TIM6->CNT < 120);
	TIM6->CNT = 0;
	while (!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9) == 0)
	{
		if(TIM6->CNT >= 500)
		{
			return 0;
		}
	}
	TIM6->CNT = 0;
	while(TIM6->CNT < 400);
	return 1;
}

void write1(void)
{
	DS18B20_mode(OUT);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	TIM6->CNT = 0;
	while(TIM6->CNT < 2);
	//TIM6->CNT = 0;
	//while(TIM6->CNT < 20);
	DS18B20_mode(IN);
	TIM6->CNT = 0;
	while(TIM6->CNT < 130);
}


void write0(void)
{
	DS18B20_mode(OUT);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	TIM6->CNT = 0;
	while(TIM6->CNT < 140);
	DS18B20_mode(IN);
}

uint8_t readbit(void)
{
	int waarde = 0;
	DS18B20_mode(OUT);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	TIM6->CNT = 0;
	while(TIM6->CNT < 8);
	DS18B20_mode(IN);
	TIM6->CNT = 0;
	while(TIM6->CNT < 16);
	if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9) == 1)
	{
		waarde = 1;
	}
	while(TIM6->CNT < 96);
	return waarde;
}

void writebyte(uint8_t value)
{
	uint8_t bit;
	uint8_t significance = 0x01;
	while (significance)
	{
		bit = value & significance;
		if (bit == 0)
		{
			write0();
		}
		else
		{
			write1();
		}
		significance <<= 1;
	}
}

uint8_t readbyte(void)
{
	uint8_t significance = 0x01;
	uint8_t data = 0x00;
	while (significance)
	{
		if (readbit())
		{
			data |= significance;
		}
		significance <<= 1;
	}
	return data;
}

void convertT(void)
{
	temp_init();
	writebyte(0xCC);	//skip rom
	writebyte(0x44);	//convertT

}

uint16_t readscratchpad(void)
{
	uint16_t temp = 0;
	uint16_t ans = 0;
	uint8_t data[9];
	temp_init();
	writebyte(0xCC);	//skip rom
	writebyte(0xBE);	//read scratchpad
	for (int i = 0; i < 9; i++)
	{
		data[i] = readbyte();
	}
	temp = data[1];
	temp <<= 8;
	temp |= data[0];
	ans = ((temp/4)*0.25)*100;
	return ans;
}

void writescratchpad(void)
{
	temp_init();
	writebyte(0xCC);	//skip rom
	writebyte(0x4E);	//write scratchpad
	writebyte(0x7D);
	writebyte(0xC9);
	writebyte(0x3F);	//10bit conversion
}

void gettemp(void)
{
	writescratchpad();
	convertT();
}

uint16_t readtemp(void)
{
	return readscratchpad();
}
/* USER CODE END 4 */

