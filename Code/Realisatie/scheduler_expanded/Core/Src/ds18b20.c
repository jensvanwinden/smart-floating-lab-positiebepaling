/*
* File: ds18b20.c
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een temperatuur gemeten en
* uitgelezen kan worden met een ds18b20 sensor. Dit is de c-file van de library.
*/

#include "ds18b20.h"


#define OUT 1
#define IN	0

/*
 * Deze functie stelt de timer in die gebruikt wordt voor de ds18b20
 */
void MX_TIM6_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 50;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 0;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  TIM6->ARR = 65535;
  HAL_TIM_Base_Start(&htim6);
}

/*
 * Deze functie bepaalt of de datapin waar de ds18b20 op is aangesloten een input of output is.
 * mode: Aan deze variabele wordt meegegeven of de pin een input of output wordt.
 */
void DS18B20_mode(int mode)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(mode == 0)
	{
		  GPIO_InitStruct.Pin = GPIO_PIN_9;
		  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		  GPIO_InitStruct.Pull = GPIO_NOPULL;
		  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	}
	else
	{
		  /*Configure GPIO pin : PB9 */
		  GPIO_InitStruct.Pin = GPIO_PIN_9;
		  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		  GPIO_InitStruct.Pull = GPIO_NOPULL;
		  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	}
}

/*
 * Deze functie zorgt ervoor dat de ds18b20 geinitialiseerd wordt.
 */
uint8_t temp_init(void)
{
	DS18B20_mode(OUT);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	TIM6->CNT = 0;
	while(TIM6->CNT < 1000);
	DS18B20_mode(IN);
	TIM6->CNT = 0;
	while(TIM6->CNT < 120);
	TIM6->CNT = 0;
	while (!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9) == 0)
	{
		if(TIM6->CNT >= 500)
		{
			return 0;
		}
	}
	TIM6->CNT = 0;
	while(TIM6->CNT < 400);
	return 1;
}

/*
 * Deze functie zorgt ervoor dat er een logische 1 wordt geschreven naar de ds18b20.
 */
void write1(void)
{
	DS18B20_mode(OUT);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	TIM6->CNT = 0;
	while(TIM6->CNT < 2);
	DS18B20_mode(IN);
	TIM6->CNT = 0;
	while(TIM6->CNT < 130);
}

/*
 * Deze functie zorgt ervoor dat er een logische 0 wordt geschreven naar de ds18b20.
 */
void write0(void)
{
	DS18B20_mode(OUT);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	TIM6->CNT = 0;
	while(TIM6->CNT < 140);
	DS18B20_mode(IN);
}

/*
 * Deze functie leest een bit uit de ds18b0 in. Dit kan een logische 1 of 0 zijn
 * afhankelijk van hetgedrage van de sensor.
 */
uint8_t readbit(void)
{
	int waarde = 0;
	DS18B20_mode(OUT);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	TIM6->CNT = 0;
	while(TIM6->CNT < 8);
	DS18B20_mode(IN);
	TIM6->CNT = 0;
	while(TIM6->CNT < 16);
	if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9) == 1)
	{
		waarde = 1;
	}
	while(TIM6->CNT < 96);
	return waarde;
}

/*
 * Deze functie zorgt ervoor dat er een byte aan data naar de ds18b20 sensor kan worden geschreven.
 * value: De waarde die geschreven wordt naar de sensor wordt hier meegegeven.
 */
void writebyte(uint8_t value)
{
	uint8_t bit;
	uint8_t significance = 0x01;
	while (significance)
	{
		bit = value & significance;
		if (bit == 0)
		{
			write0();
		}
		else
		{
			write1();
		}
		significance <<= 1;
	}
}

/*
 * Deze functie zorgt ervoor dat er een byte aan data kan worden uitgelezen uit de ds18b20 sensor.
 */
uint8_t readbyte(void)
{
	uint8_t significance = 0x01;
	uint8_t data = 0x00;
	while (significance)
	{
		if (readbit())
		{
			data |= significance;
		}
		significance <<= 1;
	}
	return data;
}

/*
 * Deze functie zorgt ervoor dat er een temperatuurmeting wordt gestart.
 */
void convertT(void)
{
	temp_init();
	writebyte(0xCC);	//skip rom
	writebyte(0x44);	//convertT

}

/*
 * Deze functie zorgt ervoor dat de gemeten temperatuur kan worden uitgelezen.
 * De gemeten temperatuur wordt gereturned.
 */
uint16_t readscratchpad(void)
{
	uint16_t temp = 0;
	uint16_t ans = 0;
	uint8_t data[9];
	temp_init();
	writebyte(0xCC);	//skip rom
	writebyte(0xBE);	//read scratchpad
	for (int i = 0; i < 9; i++)
	{
		data[i] = readbyte();
	}
	temp = data[1];
	temp <<= 8;
	temp |= data[0];
	ans = ((temp/4)*0.25)*100;
	return ans;
}

/*
 * Deze functie zorgt ervoor dat de ds18b20 op de juiste manier wordt ingesteld.
 * Hier kan een alarm worden ingesteld en de resolutie van de ds18b20 sensor.
 */
void writescratchpad(void)
{
	temp_init();
	writebyte(0xCC);	//skip rom
	writebyte(0x4E);	//write scratchpad
	writebyte(0x7D);
	writebyte(0xC9);
	writebyte(0x3F);	//10bit conversion
}

/*
 * Deze functie zorgt ervoor dat er een temperatuurmeting gedaan wordt met de juiste instellingen.
 */
void gettemp(void)
{
	writescratchpad();
	convertT();
}

/*
 * Deze functie zorgt ervoor dat de gemeten temperatuur kan worden uitgelezen.
 * De gemeten temperatuur wordt gereturned.
 */
uint16_t readtemp(void)
{
	return readscratchpad();
}


