/*
* File: task.c
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een takenlijst wordt aangemaakt
* en dat er taken worden toegevoegd. Dit is de c-file van deze libary.
*/

#include <stdlib.h>
#include <task.h>

/*
 * Deze functie zorgt ervoor dat er een takenlijst wordt aangemaakt.
 * De aangemaakte lijst wordt gereturned.
 */
Tasklist *createList(void)
{
	Tasklist *new = malloc(sizeof(Tasklist));
	new->head = NULL;
	new->tail = NULL;
	return new;
}

/*
 * Deze functie zorgt ervoor dat er taken worden toegevoegd aan de takenlijst.
 * list: De lijst waaraan de taak wordt toegevoegd.
 * period: De periode wanneer de taak herhaaldelijk wordt uitgevoerd.
 * priority: De prioriteit van de taak. Hoe lager het getal, hoe hoger de prioriteit.
 * p2Function: De pointer naar de fucntion die uitgevoerd moet worden.
 */
void addTask(Tasklist *list, int period, int priority,  void (*p2Function)())
{
	Task *new = malloc(sizeof(Task));
	new->period = period;
	new->counter = new->period;
	new->p2Function = p2Function;
	new->priority = priority;
	new->state = READY;
	new->next = NULL;
	if(list->head == NULL)
	{
		list->head = list->tail = new;
	}
	else
	{
		list->tail = list->tail->next = new;
	}
}
