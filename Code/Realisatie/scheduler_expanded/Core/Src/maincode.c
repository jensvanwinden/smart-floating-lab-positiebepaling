/*
 * File: maincode.c
 * Author: Vince de Brabander
 * Version:1.0
 * Description: In deze file staat de code geschreven voor het Smart Floating Lab.
 */
#include <gnssParse.h>
#include <lora.h>
#include "main.h"
#include "schedule.h"
#include "PID_Test.h"
#include "PID_init.h"
#include "JSN-SR04T.h"
#include "ds18b20.h"

TIM_HandleTypeDef htim11;
TIM_HandleTypeDef htim9;
SPI_HandleTypeDef hspi1;
ADC_HandleTypeDef hadc1;

#define detectieAfstand 2500

volatile int flag = 0;
Tasklist *list;
volatile int pidFlag = 0;
float afstandR = 5000;
float afstandL = 5001;

RMC new = { { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, 0, 0, 0, 0 };
Route *route, *terugroepactie;
float newTime = 0;
float lastTime = 0;
float counter = 0;
float ultracount = 0;
extern float ITerm;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM11_Init(void);
void MX_SPI1_Init(void);
void MX_ADC1_Init(void);
void MX_TIM9_Init(void);

/*
 * In deze functie worden alle functionaliteiten ge-init en de routes
 * en taken aangemaakt voor het Floating Lab.
 */
int main(void)
{
	list = createList();
	addTask(list, 100, 6, GNSS);
	addTask(list, 100, 7, PID);
	addTask(list, 1500, 5, loraSendPayload);
	addTask(list, 3000, 1, loraReceiveSettings);
	addTask(list, 50, 9, ultrasoon);
	addTask(list, 2700, 4, gettemp);
	addTask(list, 3000, 3, readDS18B20);
	addTask(list, 3000, 2, getBat);
	addTask(list, 1, 10, sleep);

	//acceptatie route
	route = createRoute();
	addLocation(route, 52.023333, 4.288771);
	addLocation(route, 52.023389, 4.289315);
	addLocation(route, 52.023128, 4.289916);
	addLocation(route, 52.023285, 4.289788);

	////////////////wateringen/////////////////////
	terugroepactie = createRoute();
	addLocation(terugroepactie, 52.023249, 4.288754);

	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_USART6_UART_Init();
	MX_SPI1_Init();
	MX_ADC1_Init();

	PID_init();
	initLora();
	initEngine();
	MX_TIM6_Init();
	MX_TIM9_Init();
	MX_TIM11_Init();

	while (1)
	{

	}
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 100;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	RCC_OscInitStruct.PLL.PLLR = 2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, task6_Pin | RESET_LORA_Pin | GPIO_PIN_8,
			GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, NSS_Pin | task8_Pin | GPIO_PIN_9 | GPIO_PIN_11,
			GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB,
			task4_Pin | task5_Pin | LED_D1_Pin | task7_Pin | GPIO_PIN_15
					| task3_Pin | task2_Pin | ONEWIRE_DEBUG_Pin,
			GPIO_PIN_RESET);

	/*Configure GPIO pins : task6_Pin RESET_LORA_Pin PC8 */
	GPIO_InitStruct.Pin = task6_Pin | RESET_LORA_Pin | GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : PC2 */
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : NSS_Pin task8_Pin PA9 PA11 */
	GPIO_InitStruct.Pin = NSS_Pin | task8_Pin | GPIO_PIN_9 | GPIO_PIN_11;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : task4_Pin task5_Pin LED_D1_Pin task7_Pin
	 PB15 task3_Pin task2_Pin ONEWIRE_DEBUG_Pin */
	GPIO_InitStruct.Pin = task4_Pin | task5_Pin | LED_D1_Pin | task7_Pin
			| GPIO_PIN_15 | task3_Pin | task2_Pin | ONEWIRE_DEBUG_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : DIO0_Pin */
	GPIO_InitStruct.Pin = DIO0_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DIO0_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PA10 */
	GPIO_InitStruct.Pin = GPIO_PIN_10;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/**
 * @brief Timer 9 Initialization Function
 * @param None
 * @retval None
 */
void MX_TIM9_Init(void)
{
	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };

	htim9.Instance = TIM9;
	htim9.Init.Prescaler = 100;
	htim9.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim9.Init.Period = 1500;
	htim9.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim9.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim9) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim9, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	TIM9->ARR = 65535;
	HAL_TIM_Base_Start(&htim9);



}

/**
 * @brief Timer 11 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM11_Init(void)
{
	htim11.Instance = TIM11;
	htim11.Init.Prescaler = 2;
	htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim11.Init.Period = 33705; //33332
	htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim11.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	if (HAL_TIM_Base_Init(&htim11) != HAL_OK) {
		Error_Handler();
	}

	TIM11->EGR = 1;
	HAL_TIM_Base_Start(&htim11);
	HAL_TIM_Base_Start_IT(&htim11);
}

/**
 * @brief ADC1 Initialization Function
 * @param None
 * @retval None
 */
void MX_ADC1_Init(void)
{
	ADC_ChannelConfTypeDef sConfig = { 0 };
	/** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
	 */
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.ScanConvMode = DISABLE;
	hadc1.Init.ContinuousConvMode = ENABLE;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 1;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&hadc1) != HAL_OK) {
		Error_Handler();
	}
	/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
	 */
	sConfig.Channel = ADC_CHANNEL_15;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief SPI 1 Initialization Function
 * @param None
 * @retval None
 */
void MX_SPI1_Init(void)
{
	/* SPI1 parameter configuration*/
	hspi1.Instance = SPI1;
	hspi1.Init.Mode = SPI_MODE_MASTER;
	hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi1.Init.NSS = SPI_NSS_SOFT;
	hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
	hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi1.Init.CRCPolynomial = 15;
	if (HAL_SPI_Init(&hspi1) != HAL_OK) {
		Error_Handler();
	}
}

/*
 * Deze functie voert de taak GNSS uit.
 */
void GNSS(void)
{
	static uint8_t nextStop = 0;
	static int i = 0;
	pidFlag = 0;
	payload.counter = counter;

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);

	if (payload.callback)
	{
		new = getGnssData(new, terugroepactie);
		payload.terugroep = 1;
	}
	else
	{
		payload.terugroep = 0;
		new = getGnssData(new, route);
	}

	if (new.haversineDist <= 3)
	{
		ITerm = 0;
		pidFlag = 1;
		if (nextStop == 0)
		{
			counter = 0;
			nextStop = 1;
			i++;
			payload.arrived_spots = i;
		}
	}

	if (counter <= 120.0)
	{
		engineMode(off);

	}
	else
	{
		if (nextStop == 1)
		{
			flag = arrived(route);
			ITerm = 0;
			nextStop = 0;
		}
	}

	if (flag == 1)
	{
		payload.destination = 1;
	}

	newTime += 0.210;
	counter += 0.210;
	ultracount += 0.210;
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
}

/*
 * Deze functie voert de taak loraSendPayload uit.
 */
void loraSendPayload(void)
{
	payload2string(&payload); //convert the payload struct to a string. both are global
	sendString(payloadString); //send the payload over lora

	newTime += 0.510;
	counter += 0.510;
	ultracount += 0.510;
}

/*
 * Deze functie voert de taak loraReceiveSettings uit.
 */
void loraReceiveSettings(void)
{
	initReceive();
	HAL_Delay(500);
	receiveSettings();
	newTime += 0.520;
	counter += 0.520;
	ultracount += 0.520;
}

/*
 * Deze functie voert de taak PID uit.
 */
void PID(void)
{
	if (pidFlag == 0)
	{
		int16_t debugCog = atoi(new.cog);
		payload.cog = debugCog;

		if (debugCog != 0)
		{
			calculatePID(new.bearing, new.cog);
		}
		else
		{
			engineMode(forward);
			servo = 150;
			payload.Servo = servo;
			payload.MotorL = leftEngine;
			payload.MotorR = rightEngine;
		}
	}
}

/*
 * Deze functie voert de taak ultrasoon uit.
 */
void ultrasoon(void) {

	static int tel = 0;
	pulseout();

	static uint8_t ultraFlag = 0;
	pidFlag = 0;
	payload.UltraDistanceRechts = afstandR;
	payload.UltraDistanceLinks = afstandL;

	if (((afstandR <= detectieAfstand && afstandR > 300)
			|| (afstandL <= detectieAfstand && afstandL > 300))
			&& ultraFlag == 0)
	{
		tel++;
		if (tel >= 5)
		{
			pidFlag = 1;
			ultraFlag = 1;
			ultracount = 0;
		}
	} else if (afstandR > detectieAfstand && afstandL > detectieAfstand)
	{
		tel = 0;
	}
	if (ultraFlag == 1)
	{
		if (ultracount <= 6.0)
		{
			servo = 150;
			ITerm = 0;
			engineMode(backward);
			pidFlag = 1;
		}
		else if (ultracount <= 9.0)
		{
			engineMode(off);
			pidFlag = 1;
		}
		else
		{
			payload.callback = 1;
			ultraFlag = 0;
			pidFlag = 0;
		}
	}
	newTime += 0.022;
	counter += 0.022;
	ultracount += 0.022;
}

/*
 * Deze functie voert de taak readDS18B20 uit.
 */
void readDS18B20(void)
{
	payload.temp = readtemp();
	newTime += 0.013;
	counter += 0.013;
	ultracount += 0.013;
}

/*
 * Deze functie voert de taak getBat uit.
 */
void getBat(void)
{
	uint32_t static value;
	static float vBat = 0;
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 1);
	value = HAL_ADC_GetValue(&hadc1);
	vBat = (value * (3.27 / 4096)) / 0.401226;
	payload.vBat = vBat;
}

/*
 * Deze functie voert de taak sleep uit.
 */
void sleep(void)
{

}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{

}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
