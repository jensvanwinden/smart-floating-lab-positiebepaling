/*
* File: schedule.c
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een taak kan worden uitgevoerd en
* dat de volgende taak wordt bepaald a.d.h.v. de hoogste prioriteit. Dit is de c-file van deze libary.
*/

#include "schedule.h"
#include "main.h"

/*
 * Deze functie zorgt ervoor dat de volgende taak die wordt uitgevoerd
 * de taak is die ready is met de hoogste prioriteit.
 * list: de lijst met taken.
 * Deze functie returned de taak die ready is met de hoogste prioriteit.
 */
Tasklist *nextTask(Tasklist *list)
{
	Task *loop = list->head;
	Task *prio = {0};
	prio->priority = 11;
	while(loop != NULL)
	{
		if(loop->state == READY && prio->priority > loop->priority)
		{
			prio = loop;
		}
		loop = loop->next;
	}
	return prio;
}

/*
 * Deze functie voert de huidige taak uit en zorgt ervoor dat zijn counter gereset wordt.
 * task: De taak die wordt uitgevoerd.
 */
void runCurrentTask(Task *task)
{
	task->p2Function();
	task->state = WAITING;
	task->counter = task->period;
}

