/*
* File: route.c
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een route wordt aangemaakt en locaties
* kunnen worden toegevoegd m.b.v. een gelinkte lijst. Wanneer er een locaties bereikt is
* zal de volgende locatie de huidige worden. Dit is de c-file van deze libary.
*/

#include <debug.h>
#include <route.h>
#include <lora.h>
#include <stddef.h>
#include <stdlib.h>

/*
 * Deze functie creert een route zonder locaties.
 * Deze functie returned de aangemaakt route.
 */
Route *createRoute(void)
{
	Route *new = malloc(sizeof(Route));
	new->end = NULL;
	new->currentLocation = NULL;
	return new;
}

/*
 * Deze functie voegt een locatie toe aan een route.
 * actual: Dit is de route waaraan locaties worden toegevoegd.
 * lat: De latitude van de toegevoegde locatie.
 * lon: De longitude van de toegevoegde locatie.
 */
void addLocation(Route *actual, float lat, float lon)
{
	Location *new = malloc(sizeof(Location));
	new->lat = lat;
	new->lon = lon;
	new->next = NULL;
	if(actual->currentLocation == NULL)
	{
		actual->currentLocation = actual->end = new;
	}
	else
	{
		actual->end = actual->end->next = new;
	}
}

/*
 * Deze functie zorgt ervoor dat er naar een volgende locatie gegaan kan
 * worden wanneer deze functie wordt uitgevoerd.
 * route: De route waarin de wijzingen worden aangebracht.
 * Deze functie returned een 1 als de laatste locatie van de route bereikt is.
 * Wanneer dit niet het geval is wordt een 0 gereturned.
 */
uint8_t arrived(Route *route)
{

	if(route->currentLocation == route->end)
	{
		return 1;
	}
	route->currentLocation = route->currentLocation->next;

	return 0;
}
