/*
* File: task.h
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat er een takenlijst wordt aangemaakt
* en dat er taken worden toegevoegd. Dit is de h-file van deze libary.
*/

#ifndef TASK_H_
#define TASK_H_

enum taskState{RUNNING, READY, WAITING};

typedef struct TaskNode
{
	void (*p2Function)();
	int priority;
	int period;
	int counter;
	enum taskState state;
	struct TaskNode *next;
}Task;

typedef struct
{
	Task *head;
	Task *tail;
}Tasklist;


Tasklist *createList(void);
void addTask(Tasklist *list, int period, int priority,  void (*p2Function)());
void toggleLed(void);

#endif /* TASK_H_ */
