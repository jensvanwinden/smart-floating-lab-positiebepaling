/*
 * JSN-SR04T.h
 *
 *  Created on: Jan 6, 2020
 *      Author: Vince
 */

#ifndef INC_JSN_SR04T_H_
#define INC_JSN_SR04T_H_

#include "main.h"

UART_HandleTypeDef huart1;

void pulseout(void);

#endif /* INC_JSN_SR04T_H_ */
