/*
* 	File: debug.c
* 	Version:1.0
*  	Author: Michael van der Bend, Jens van Winden, Vince de Brabander en Chiel-Jan Harleman
*  	Description: Deze library is gebruikt om tijdens het programmeren debugdata via de uart uit te kunnen lezen.
*  	Dit is de H-file van de library.
*/

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

#include <main.h>
#include <main.h>



UART_HandleTypeDef huart6;
void MX_USART6_UART_Init(void);
void debugCharPrint(char sendMessage[]);
void debugIntPrint(int message);
void debugPrintEnter(void);
void debugFloatPrint(float message);
void debugRead(void);

#endif /* INC_DEBUG_H_ */
