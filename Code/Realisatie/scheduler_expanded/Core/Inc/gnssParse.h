/*
* File: gnssParse.h
* Author: Vince de Brabander
* Version:1.0
* Description: Deze library zorgt ervoor dat alle benodigde data uit de GNSS module wordt
* opgeslagen in het juiste struct. Dit is de h-file van deze libary.
*/

#ifndef INC_GNSSPARSE_H_
#define INC_GNSSPARSE_H_

#include <debug.h>
#include <rmc.h>
#include <route.h>
#include "main.h"

void initGnss(void);
RMC getGnssData(RMC data, Route *route);

#endif /* INC_GNSSPARSE_H_ */
