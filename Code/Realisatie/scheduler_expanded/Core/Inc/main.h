/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @author			: Vince de Brabander
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif


#include "stm32f4xx_hal.h"
#include "schedule.h"

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
void Error_Handler(void);
void GNSS(void);
void loraSendPayload(void);
void loraReceiveSettings(void);
void PID(void);
void ultrasoon(void);
void readDS18B20(void);
void getBat(void);
void sleep(void);

#define task6_Pin GPIO_PIN_0
#define task6_GPIO_Port GPIOC
#define NSS_Pin GPIO_PIN_4
#define NSS_GPIO_Port GPIOA
#define RESET_LORA_Pin GPIO_PIN_4
#define RESET_LORA_GPIO_Port GPIOC
#define task4_Pin GPIO_PIN_0
#define task4_GPIO_Port GPIOB
#define task5_Pin GPIO_PIN_1
#define task5_GPIO_Port GPIOB
#define LED_D1_Pin GPIO_PIN_12
#define LED_D1_GPIO_Port GPIOB
#define task7_Pin GPIO_PIN_14
#define task7_GPIO_Port GPIOB
#define DIO0_Pin GPIO_PIN_9
#define DIO0_GPIO_Port GPIOC
#define task8_Pin GPIO_PIN_8
#define task8_GPIO_Port GPIOA
#define task3_Pin GPIO_PIN_6
#define task3_GPIO_Port GPIOB
#define task2_Pin GPIO_PIN_7
#define task2_GPIO_Port GPIOB
#define ONEWIRE_DEBUG_Pin GPIO_PIN_9
#define ONEWIRE_DEBUG_GPIO_Port GPIOB


#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
