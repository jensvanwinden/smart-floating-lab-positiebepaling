/*
 * gnssParse.h
 *
 *  Created on: Dec 19, 2019
 *      Author: Vince
 */

#ifndef SRC_GNSSPARSE_H_
#define SRC_GNSSPARSE_H_

#include "main.h"
#include "debug.h"
#include "rmc.h"
#include "route.h"

void initGnss(void);
RMC getGnssData(RMC data, Route *route,  char in[]);

#endif /* SRC_GNSSPARSE_H_ */
