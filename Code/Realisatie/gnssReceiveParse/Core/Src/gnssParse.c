#include "gnssParse.h"



RMC getGnssData(RMC data, Route *route,  char in[])
{
	getRawRMC(in);
	debugCharPrint(in);
	data = parseRMC(in);
	data.bearing = getBearing(data.latConv, data.lonConv, route->currentLocation->lat, route->currentLocation->lon);
	data.haversineDist = getDistance(data.latConv, data.lonConv, route->currentLocation->lat, route->currentLocation->lon);

	return data;
}
