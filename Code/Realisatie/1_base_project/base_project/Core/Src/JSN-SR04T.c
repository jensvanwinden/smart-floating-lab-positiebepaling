/*
 * rmc.c
 *
 *  Created on: 22 nov. 2019
 *      Author: Jens
 */

#include "JSN-SR04T.h"
#include <stdio.h>




//int main(void)
//{
//	uint16_t distance = getDistance();
//}

uint16_t detectObstacle(void)
{
	static uint8_t startMessage[1] = { 0x55 };
	static uint8_t rxBuffer[4] = { 0 };
	static uint16_t ans = 0;

	HAL_UART_Transmit_IT(&huart1, startMessage, 1); //write starting commando for sensor
	HAL_UART_Receive(&huart1, rxBuffer, 4, 0x00FF); //receive data, save in rxBuffer
	ans = (*(rxBuffer + 1) << 8) + *(rxBuffer + 2); //calculate distance in mm.
	
	return ans;
}

void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}
