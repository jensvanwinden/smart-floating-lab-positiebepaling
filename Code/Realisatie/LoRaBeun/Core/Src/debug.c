/*
 * debug.c
 *
 *  Created on: Dec 2, 2019
 *      Author: Vince
 */

#include "debug.h"
#include <stdlib.h>
#include <stdio.h>
#include "Lora.h"
#include <string.h>

void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */

void debugCharPrint(char sendMessage[])
{
	//char newline[2] = "\r\n";
	HAL_UART_Transmit(&huart1, (uint8_t*) sendMessage, strlen(sendMessage), HAL_MAX_DELAY);
	//HAL_UART_Transmit(&huart1, (uint8_t*) newline, 2, HAL_MAX_DELAY);
}


void debugIntPrint(int message)
{
	char buffer[20] = {0};
	itoa(message,buffer,10);
	debugCharPrint(buffer);
}

void debugPrintEnter(void)
{
	char newline[2] = "\r\n";
	HAL_UART_Transmit(&huart1, (uint8_t*) newline, 2, HAL_MAX_DELAY);
}

void debugRead(void)
{
	char rxBuffer[1];
	static char commandRxBuffer[3] = "000";
	static char command[6];
	static uint8_t i = 1;
	if(HAL_UART_Receive(&huart1, (uint8_t*)rxBuffer, 1, 100) == HAL_OK)
	{
		//HAL_UART_Transmit(&huart1, (uint8_t*)rxBuffer, 1, 100);
		if (rxBuffer[0] == '!')
		{
			debugCharPrint("Executing: ");
			HAL_UART_Transmit(&huart1, (uint8_t*)commandRxBuffer, 3, 100);
			if (commandRxBuffer[0] == 'R' && commandRxBuffer[1] == 'S' && commandRxBuffer[2] == 'T')
			{
				debugCharPrint("  -> RESET \n");
				payload.reset = 1;
				debugCharPrint(" -> payload.reset = 1 \n");
			}
			else if (commandRxBuffer[0] == 'C' && commandRxBuffer[1] == 'M' && commandRxBuffer[2] == 'D')
			{
				debugCharPrint("  -> COMMAND \n");
				debugCharPrint("Please enter a command (return = RTN, ...., ....) within 10s\n");
				int i=0;

				while(HAL_UART_Receive(&huart1, (uint8_t*)command, 3, 100) != HAL_OK)
				{
					i++;
					HAL_Delay(1);
					if(i > 100){
						debugCharPrint("Timed out, returning to main menu \n");
						return;
					}
				}

				debugCharPrint("Executing: ");
				debugCharPrint(command);
				HAL_Delay(30);
				if (command[0] == 'R' && command[1] == 'T' && command[2] == 'N')
				{
					payload.callback = 1;
					debugCharPrint(" -> payload.callback = 1 \n");
				}
//				else if
//				{
//
//				}
				else
				{
					debugCharPrint("-> Unknown command: ");
					HAL_UART_Transmit(&huart1, (uint8_t*)command, 3, 100);
					debugCharPrint(" ,  returning to main menu \n");
				}
			}
			else if (commandRxBuffer[0] == 'W' && commandRxBuffer[1] == 'R' && commandRxBuffer[2] == 'T')
			{
				debugCharPrint("  -> WRITE \n");
				//Code to write certain variable here
				debugCharPrint("Please enter a variable (P, I, D) within 10s\n");
				int i=0;

				while(HAL_UART_Receive(&huart1, (uint8_t*)command, 1, 100) != HAL_OK)
				{
					i++;
					HAL_Delay(1);
					if(i > 100){
						debugCharPrint("Timed out, returning to main menu \n");
						return;
					}
				}

				debugCharPrint("Executing: ");
				debugCharPrint(command);
				HAL_Delay(30);
				if (command[0] == 'P')
				{
					payload.variable = 1;
					debugCharPrint(" -> writing kP \n");
					debugCharPrint("Please enter a integer value, followed by a decimal value (i,dddd)\n");
					static char integer[1], decimal[1];
					int j=0;
					while(HAL_UART_Receive(&huart1, (uint8_t*)integer, 1, 100) != HAL_OK)
					{
						j++;
						HAL_Delay(1);
						if(j > 10000){
							debugCharPrint("Timed out, returning to main menu \n");
							return;
						}
					}
					debugCharPrint("Integer =  ");
					debugCharPrint(integer);
					int k=0;
					while(HAL_UART_Receive(&huart1, (uint8_t*)decimal, 4, 100) != HAL_OK)
					{
						k++;
						HAL_Delay(1);
						if(k > 10000){
							debugCharPrint("Timed out, returning to main menu \n");
							return;
						}
					}
					debugCharPrint("Decimal =  ");
					debugCharPrint(decimal);
					HAL_Delay(30);
					debugCharPrint("kP =  ");
					debugCharPrint(integer);debugCharPrint(",");debugCharPrint(decimal);
					debugPrintEnter();
					payload.value = atoi(integer)+atoi(decimal)/10000.0;
					debugCharPrint("Done, returning to main menu \n");
					return;
				}
				else if(command[0] == 'I')
				{
					payload.variable = 2;
					debugCharPrint(" -> writing kI \n");
					debugCharPrint("Please enter a integer value, followed by a decimal value (i,dddd)\n");
					static char integer[1], decimal[1];
					int j=0;
					while(HAL_UART_Receive(&huart1, (uint8_t*)integer, 1, 100) != HAL_OK)
					{
						j++;
						HAL_Delay(1);
						if(j > 10000){
							debugCharPrint("Timed out, returning to main menu \n");
							return;
						}
					}
					debugCharPrint("Integer =  ");
					debugCharPrint(integer);
					int k=0;
					while(HAL_UART_Receive(&huart1, (uint8_t*)decimal, 4, 100) != HAL_OK)
					{
						k++;
						HAL_Delay(1);
						if(k > 10000){
							debugCharPrint("Timed out, returning to main menu \n");
							return;
						}
					}
					debugCharPrint("Decimal =  ");
					debugCharPrint(decimal);
					HAL_Delay(30);
					debugCharPrint("kI =  ");
					debugCharPrint(integer);debugCharPrint(",");debugCharPrint(decimal);
					debugPrintEnter();
					payload.value = atoi(integer)+atoi(decimal)/10000.0;
					debugCharPrint("Done, returning to main menu \n");
					return;
				}
				else if(command[0] == 'D')
				{
					payload.variable = 3;
					debugCharPrint(" -> writing kD \n");
					debugCharPrint("Please enter a integer value, followed by a decimal value (i,dddd)\n");
					static char integer[1], decimal[1];
					int j=0;
					while(HAL_UART_Receive(&huart1, (uint8_t*)integer, 1, 100) != HAL_OK)
					{
						j++;
						HAL_Delay(1);
						if(j > 10000){
							debugCharPrint("Timed out, returning to main menu \n");
							return;
						}
					}
					debugCharPrint("Integer =  ");
					debugCharPrint(integer);
					int k=0;
					while(HAL_UART_Receive(&huart1, (uint8_t*)decimal, 4, 100) != HAL_OK)
					{
						k++;
						HAL_Delay(1);
						if(k > 10000){
							debugCharPrint("Timed out, returning to main menu \n");
							return;
						}
					}
					debugCharPrint("Decimal =  ");
					debugCharPrint(decimal);
					HAL_Delay(30);
					debugCharPrint("kD =  ");
					debugCharPrint(integer);debugCharPrint(",");debugCharPrint(decimal);
					debugPrintEnter();
					payload.value = atoi(integer)+atoi(decimal)/10000.0;
					debugCharPrint("Done, returning to main menu \n");
					return;
				}
				else
				{
					debugCharPrint("-> Unknown command: ");
					HAL_UART_Transmit(&huart1, (uint8_t*)command, 3, 100);
					payload.variable = 0;
					debugCharPrint(" ,  returning to main menu \n");
				}
			}
			else if (commandRxBuffer[0] == 'D' && commandRxBuffer[1] == 'N' && commandRxBuffer[2] == 'E')
						{
							debugCharPrint("  -> DONE \n");
							payload.ready = 1;
							debugCharPrint(" -> Done configuring, will attempt sending settings \n");
						}
			else
			{
				debugCharPrint("-> Unknown command: ");
				HAL_UART_Transmit(&huart1, (uint8_t*)commandRxBuffer, 3, 100);
				debugCharPrint(" ,  returning to main menu \n");
			}
			i = 0;
		}
		commandRxBuffer[i-1] = rxBuffer[0];
		i++;

	}
}



//void debugFloatPrint(float message)
//{
//	char buffer[10] = {0};
//	gcvt(message, 5, buffer);
//	//sprintf(buffer, "%g", message);
//	debugCharPrint(buffer);
//}

