/*
 * rmc.h
 *
 *  Created on: 22 nov. 2019
 *      Author: Jens
 */

#ifndef JSN-SR05T_H_
#define JSN-SR05T_H_

	uint8_t startMessage[1] = { 0x55 };
	uint8_t rxBuffer[4] = { 0 };
	uint16_t ans = 0;

	uint16_t getDistance();

#endif /* JSN-SR05T_H_ */
