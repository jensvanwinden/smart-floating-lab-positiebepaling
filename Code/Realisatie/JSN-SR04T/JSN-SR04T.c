/*
 * rmc.c
 *
 *  Created on: 22 nov. 2019
 *      Author: Jens
 */

#include "JSN-SR05T.h"
#include <stdio.h>

//int main(void)
//{
//	uint16_t distance = getDistance();
//}

uint16_t getDistance()
{
	HAL_UART_Transmit_IT(&huart1, startMessage, 1); //write starting commando for sensor
	HAL_UART_Receive(&huart1, rxBuffer, 4, 0x00FF); //receive data, save in rxBuffer
	ans = (*(rxBuffer + 1) << 8) + *(rxBuffer + 2); //calculate distance in mm.
	
	return ans;
}
