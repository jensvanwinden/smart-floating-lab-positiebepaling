/*
 * lora.h
 *
 *  Created on: Dec 20, 2019
 *      Author: Vince
 */

#ifndef SRC_LORA_H_
#define SRC_LORA_H_

typedef struct
{
	int16_t distance;
	int16_t bearing;
	int16_t cog;
	float lat;
	float lon;

	float PTerm;
	float ITerm;
	float DTerm;
	float errorAngle;
	float PIDperc;
	float MotorL;
	float MotorR;
	int16_t Servo;
	float SampleTime;

	int reset;
	int callback;
	int variable; 	//nothing = 0 kp=1, ki=2, kd=3
	float value;

	int ready;

}payloadTag;
payloadTag payload;


void initLora(void);
void initReceive(void);
char *receiveString(void);
char *receiveInt(void);
void sendString(char message[]);
void sendInt(int message[]);
void payload2string(payloadTag *payload);
void setting2string(payloadTag *payload);
void receiveSettings(void);


#define timeOut 2000



int ret;
char buffer[200];
char payloadBuffer[200];
char settingsBuffer[200];

int message_length;
enum type {Sender = 1, Receiver = 0};
enum payloadtype {Settings = 1, Payload = 0};

//Change this when uploading to define sender and receiver PCB


char payloadString[200];
char settingString[200];

#endif /* SRC_LORA_H_ */
