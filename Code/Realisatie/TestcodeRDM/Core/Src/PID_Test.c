/*
 * PID_Test.c
 *
 *  Created on: 6 dec. 2019
 *      Author: ToBeEdited
 */


#ifndef SRC_PID_TEST_C_
#define SRC_PID_TEST_C_

#include "main.h"
#include <stdio.h>
#include <math.h>
#include "PID_Test.h"
#include "debug.h"
#include "stdlib.h"
#include "lora.h"


float Kp = 4.0;
float KiSet = 1.0;
float Ki = 0.02;
float Kd = 2.5;
#define maxITerm 150.0
#define minITerm -150.0
#define PWMPeriod 2000.0 //Chosen period
#define invertSteering 1

float lastError = 0;
int8_t engineSteering = 1;



int16_t setTo360(int16_t degree)
{
	if(degree < 0)
	{
		degree = degree + 360;
	}

	return degree;
}

float degreeToPercentage(int16_t degree)
{
	float degreeF = (float)degree;
	float percentage = degreeF * (100.0 / 180.0);

//	if(invertSteering == 1)							naar andere plek
//	{
//		percentage = percentage*-1;
//	}

//	debugCharPrint(" degreegTo% = ");
//	debugFloatPrint(percentage);
//	debugPrintEnter();
	return percentage;
}

int16_t determineError(int16_t setpointAngle, int16_t orientationAngle)
{
	int16_t errorAngle = 0;
	int16_t angle1	= 0;
	int16_t angle2 = 0;
	errorAngle = setTo360(setpointAngle) - setTo360(orientationAngle);

//	debugCharPrint(" Setpoint = ");
//	debugIntPrint(setTo360(setpointAngle));
//	debugPrintEnter();
//	debugCharPrint(" heading = ");
//	debugIntPrint(setTo360(orientationAngle));
//	debugPrintEnter();

	if(errorAngle < -180 || errorAngle > 180)
	{
		angle1 = errorAngle + 360;
		angle2 = errorAngle - 360;

		if(pow(angle1,2) > pow(angle2,2))
		{
//			debugCharPrint(" Errorangle = ");
//			debugFloatPrint(angle2);
//			debugPrintEnter();
			payload.errorAngle = angle2;
			return angle2;
		}
		else
		{
//			debugCharPrint(" Errorangle = ");
//			debugFloatPrint(angle1);
//			debugPrintEnter();
			payload.errorAngle = angle1;
			return angle1;
		}
	}
	else
	{
//		debugCharPrint(" Errorangle = ");
//		debugFloatPrint(errorAngle);
//		debugPrintEnter();
		payload.errorAngle = errorAngle;
		return errorAngle;
	}
}

void calculatePID(float bearing, char* cog)
{
	static float newError = 0;
	static float ITerm = 0;
	float DTerm = 0;
	float PTerm = 0;
	float totalGain = 0;

	int16_t setPoint = (int16_t)bearing;
    int16_t	orientatie = atoi(cog);

	payload.bearing = setTo360(setPoint);
	payload.cog = setTo360(orientatie);

    newError = degreeToPercentage(determineError(setPoint, orientatie));  				// with bearing and cog
	//newError = degreeToPercentage(determineError(setpointTest, orientatieTest));

    extern float newTime;
	extern float lastTime;
	float sampleTimeF = newTime - lastTime;
	payload.SampleTime = sampleTimeF;

	PTerm =  	Kp*(newError * sampleTimeF);
	DTerm = 	Kd*((lastError - newError) / sampleTimeF);
	ITerm += 	Ki*((KiSet*newError) * sampleTimeF);

	if(ITerm > maxITerm)
	{
		ITerm = maxITerm;
	}
	else if(ITerm < minITerm)
	{
		ITerm = minITerm;
	}

	totalGain = PTerm + DTerm + ITerm;

		if(invertSteering == 1)
		{
			totalGain = totalGain*-1;
		}

	payload.PIDperc = totalGain;

	if((totalGain > 100) && (lastError > 0) && (newError > 0))
	{
		KiSet = 0.0;
	}
	else if((totalGain < -100) && (lastError < 0) && (newError < 0))
	{
		KiSet = 0.0;
	}
	else
	{
		KiSet = 1.0;
	}


	payload.PTerm = PTerm;
	payload.ITerm = ITerm;
	payload.DTerm = DTerm;

	percentageToPWMvoid(totalGain);
	lastTime = newTime;
	lastError = newError;

	//payload.kiSet = KiSet;
	//percentageToPWMvoid(totalGain);				comment for debugging

	/*DEBUG
	 * -------------------------------------------------------------------------------
	 */
	//uint16_t test = percentageToPWM(totalGain);


//	debugCharPrint(" Sampletime = ");
//	debugFloatPrint(sampleTimeF);
//	debugPrintEnter();
//	debugCharPrint(" newTime = ");
//	debugFloatPrint(newTime);
//	debugPrintEnter();
//	debugCharPrint(" lastTime = ");
//	debugFloatPrint(lastTime);
//	debugPrintEnter();
//	debugCharPrint(" KiSet = ");
//	debugFloatPrint(KiSet);
//	debugPrintEnter();
//	debugCharPrint(" newError = ");
//	debugFloatPrint(newError);
//	debugPrintEnter();
//	debugCharPrint(" lastError = ");
//	debugFloatPrint(lastError);
//	debugPrintEnter();
//	debugCharPrint(" PTerm = ");
//	debugIntPrint(PTerm);
//	debugPrintEnter();
//	debugCharPrint(" DTerm = ");
//	debugIntPrint(DTerm);
//	debugPrintEnter();
//	debugCharPrint(" ITerm = ");
//	debugIntPrint(ITerm);
//	debugPrintEnter();
//	debugCharPrint(" totalPID% = ");
//	debugFloatPrint(totalGain);
//	debugPrintEnter();
//	debugCharPrint(" % to pwm = ");
//	debugIntPrint(test);
//	debugPrintEnter();
	/*
	 * --------------------------------------------------------------------------------
	 */
}


void percentageToPWMvoid(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;
	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	if(engineSteering == 1)
	{
		//engineMode(forwardHalfThrottle);

		if(PIDPercentage > 100)
		{
			PIDPercentage = 100;
		}

		else if (PIDPercentage < -100)
		{
			PIDPercentage = -100;
		}

		if(PIDPercentage <= 0) //links sturen
		{
			rightEngine = PWMPeriod*enginePowerLimit.enginePowerLimit;
			leftEngine = (PWMPeriod*enginePowerLimit.enginePowerLimit) + (fabs(PIDPercentage)*enginePowerLimit.engineDivideFactor); //
			payload.MotorL = leftEngine;
			payload.MotorR = rightEngine;
//			debugCharPrint(" LeftEngine = ");
//			debugIntPrint(leftEngine);
//			debugPrintEnter();
//			debugCharPrint(" RightEngine = ");
//			debugIntPrint(rightEngine);
//			debugPrintEnter();
		}

		else if(PIDPercentage > 0) //rechts sturen
		{
			leftEngine = PWMPeriod*enginePowerLimit.enginePowerLimit;
			rightEngine = (PWMPeriod*enginePowerLimit.enginePowerLimit) + (fabs(PIDPercentage)*enginePowerLimit.engineDivideFactor); //
			payload.MotorL = leftEngine;
			payload.MotorR = rightEngine;
//			debugCharPrint(" LeftEngine = ");
//			debugIntPrint(leftEngine);
//			debugPrintEnter();
//			debugCharPrint(" RightEngine = ");
//			debugIntPrint(rightEngine);
//			debugPrintEnter();
		}
	}
	else
	{
		engineMode(forwardHalfThrottle);
//		payload.newTime = leftEngine;
//		payload.kiSet = rightEngine;
//		debugCharPrint(" LeftEngine = ");
//		debugIntPrint(leftEngine);
//		debugPrintEnter();
//		debugCharPrint(" RightEngine = ");
//		debugIntPrint(rightEngine);
//		debugPrintEnter();
	}

	uint16_t PWMTimeI = (uint16_t)PWMTimeF; 					//Debug

	if(PWMTimeI > 200)
	{
		PWMTimeI = 200;
		servo = PWMTimeI;
		payload.Servo = PWMTimeI;
		//payload.PWMPeriod =
	}
	else if(PWMTimeI < 100)
	{
		PWMTimeI = 100;
		servo = PWMTimeI;
		payload.Servo = PWMTimeI;
	}
	else
	{
		servo = PWMTimeI; 										//Servo
		payload.Servo = PWMTimeI;
	}


}


int16_t percentageToPWM(float PIDPercentage) //negative percentage to the left, positive to the right
{
	float PWMTimeF;

	if(PIDPercentage > 100)
	{
		PIDPercentage = 100;
	}
	if(PIDPercentage < -100)
	{
		PIDPercentage =-100;
	}

	PWMTimeF = (PIDPercentage * (0.00025*PWMPeriod)) + (0.075*PWMPeriod); //0.075% of total PWMPeriod is idle state servo&ESC

	int16_t PWMTimeI = (int16_t)PWMTimeF;
	return PWMTimeI;
}

void engineMode(uint8_t engineMode)
{
	switch(engineMode)
	{
		case forward:
			{
				rightEngine= PWMPeriod*0.05; //links
				leftEngine= PWMPeriod*0.05; //rechts
			}
		break;
		case forwardHalfThrottle:
			{
				rightEngine= PWMPeriod*0.0625; //links
				leftEngine= PWMPeriod*0.0625; //rechts
			}
		break;
		case off:
		{
				rightEngine= PWMPeriod*0.075;
				leftEngine= PWMPeriod*0.075;
		}
		break;
		case backward:
		{
				rightEngine= PWMPeriod*0.1;
				leftEngine= PWMPeriod*0.1;
		}
				break;
		case leftTurn:								//Debug case
		{
			rightEngine= PWMPeriod*0.075;
			leftEngine= PWMPeriod*0.1;
		}
		break;
		case rightTurn:								//Debug case
		{
			rightEngine= PWMPeriod*0.1;
			leftEngine= PWMPeriod*0.075;
		}
		break;
	}
}

void defineEnginePower(int8_t percentage)
{
	enginePowerLimit.enginePowerLimit = (percentage * -0.00025) + 0.075;
	enginePowerLimit.engineDivideFactor = (150-(enginePowerLimit.enginePowerLimit*PWMPeriod))/100;
}

void initEngine()
{
	defineEnginePower(100);
	rightEngine = 150; //links
	leftEngine = 150; //rechts
	servo = 150; //Servo
	payload.MotorL = leftEngine;
	payload.MotorR = rightEngine;
	payload.Servo = servo;
	HAL_Delay(2000);
}

#endif /* SRC_PID_TEST_C_ */
