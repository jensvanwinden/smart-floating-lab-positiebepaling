/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "PID_Test.h"
#include "debug.h"
#include "PID_init.h"
#include "rmc.h"
#include "route.h"
#include "lora.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"

float newTime = 0.0;
float lastTime = 0.0;
//float debugBearing = 200;


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/


/* USER CODE BEGIN PV */
SPI_HandleTypeDef hspi1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
//static void MX_TIM5_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */
enum type LoraType = Sender;
/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
      HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */
  MX_USART2_UART_Init();
  MX_USART6_UART_Init();
  MX_SPI1_Init();
  PID_init();
  initEngine();
  initLora();

  if (LoraType == Receiver)
  {
  	initReceive();
  }

  char in[NMEA_LENGHT_FIX] = {0};
  //char in[NMEA_LENGHT_FIX] = "$GNRMC,101359.00,A,5154.59071,N,00427.75202,E,2.116,60.21,221119,,,D*4F";
  int flag = 0;
  RMC new = {{0}, {0}, {0}, {0}, {0}, {0}, 0, 0, 0, 0};

  Route *route = createRoute();
  //--------------------------------------- DOBBEPLAS
//  addLocation(route, 52.035033, 4.403159);
//  addLocation(route, 52.034817, 4.402706);
//  addLocation(route, 52.035327, 4.403025);

  //--------------------------------------- Jens thuis rechtdoor
//  addLocation(route, 52.017626, 4.421510);
//  addLocation(route, 52.018231, 4.421244);
//  addLocation(route, 52.018825, 4.420340);

  //--------------------------------------- alblasserdam Dam
//  addLocation(route, 51.859489, 4.659862);
//  addLocation(route, 51.860227, 4.659937);

  //--------------------------------------- Jens thuis 2
  addLocation(route, 52.017626, 4.421510);
  addLocation(route, 52.018231, 4.421244);
  addLocation(route, 52.018825, 4.420340);






  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {

		//HAL_Delay(1500);
		//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET); //D4
		//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_9);
		getRawRMC(in);
		new = parseRMC(in);
		new.bearing = getBearing(new.latConv, new.lonConv,route->currentLocation->lat, route->currentLocation->lon);
		new.haversineDist = getDistance(new.latConv, new.lonConv,route->currentLocation->lat, route->currentLocation->lon);


		payload.distance = new.haversineDist;
		payload.lat = new.latConv;
		payload.lon = new.lonConv;

//		debugCharPrint(" Latitude = ");
//		debugFloatPrint(new.latConv);
//		debugPrintEnter();
//
//		debugCharPrint(" Longitude = ");
//		debugFloatPrint(new.lonConv);
//		debugPrintEnter();

//			static int test = 0;
//
//			char debugCharCog[16];
//			sprintf(debugCharCog, "%d", test);
//			//debugCharCog = itoa(test, buffer, 10);
//			//snprintf(debugCharCog, 3, "%d", test);
//			calculatePID(debugBearing, debugCharCog);
//
//			test -=10;
//			if (test < -360)
//			{
//				test = 0;
//			}

		int16_t	debugCog = atoi(new.cog);
		payload.cog = debugCog;
//		debugIntPrint(debugCog);
//		debugPrintEnter();

		//calculatePID(new.bearing, new.cog);

		if(debugCog != 0)
		{
			calculatePID(new.bearing, new.cog);
		}
		else
		{
			engineMode(forward);
			servo = 150;
			payload.Servo = servo;
			payload.MotorL = leftEngine;
			payload.MotorR = rightEngine;
		}

		if(new.haversineDist <= 5)
			  {
				engineMode(off);
				HAL_Delay(10000);
				flag = arrived(route);
				//HAL_Delay(10000);

				  if(flag == 1)
				  {
//					  debugCharPrint("Eindbestemming bereikt");
//					  debugPrintEnter();
				  }
			  }

//		debugCharPrint(" Distance = ");
//		debugIntPrint(new.haversineDist);
//		debugPrintEnter();
//		debugPrintEnter();
//
//		debugPrintEnter();
//		debugPrintEnter();
//		debugPrintEnter();
//		debugPrintEnter();
//		debugPrintEnter();


		if (LoraType == Receiver)
		{
			//check of module has any pending messages
			receiveString(); //returns char * to char []
		}
		else if (LoraType == Sender)
		{
			payload2string(&payload); //convert the payload struct to a string. both are global
			sendString(payloadString); //send the payload over lora
		}

		//HAL_Delay(1000);
		newTime += 0.6;
		if (newTime >= 1000) {
			newTime = 0.0;
			lastTime = 0;
		}

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 80;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */

static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 15;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}
/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(NSS_GPIO_Port, NSS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RESET_LORA_GPIO_Port, RESET_LORA_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_D1_Pin|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin : NSS_Pin */
  GPIO_InitStruct.Pin = NSS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(NSS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : RESET_LORA_Pin */
  GPIO_InitStruct.Pin = RESET_LORA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(RESET_LORA_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_D1_Pin PB9 */
  GPIO_InitStruct.Pin = LED_D1_Pin|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : DIO0_Pin */
  GPIO_InitStruct.Pin = DIO0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(DIO0_GPIO_Port, &GPIO_InitStruct);

}

//}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
