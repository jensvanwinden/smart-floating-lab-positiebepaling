/*
 * route.h
 *
 *  Created on: 5 dec. 2019
 *      Author: Vince
 */

#ifndef ROUTE_H_
#define ROUTE_H_

#include <stdint.h>

typedef struct LocationTag
{
	float lat;
	float lon;
	struct LocationTag *next;
}Location;

typedef struct
{
	Location *currentLocation;
	Location *end;
}Route;

Route *createRoute(void);
void addLocation(Route *actual, float lat, float lon);
uint8_t arrived(Route *route);


#endif /* ROUTE_H_ */
