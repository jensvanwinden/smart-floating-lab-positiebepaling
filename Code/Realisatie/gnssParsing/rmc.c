/*
 * rmc.c
 *
 *  Created on: 22 nov. 2019
 *      Author: Vince
 */

#include "rmc.h"
#include <stdio.h>
#include <stdlib.h>

RMC parseRMC(char data[])
{
	RMC new = {{0}, {0}, {0}, {0}, {0}, 0, 0};
	int offset = 0;

	offset = searchData(data,',',offset);
	offset = copyData(data+offset,new.time,',');
	if(*(data+17) == 'A')
	{
		offset = 19;
		offset = offset + copyData(data+19,new.lat,',');
		offset = searchData(data,',',offset);
		offset = offset + copyData(data+offset,new.lon,',');
		offset = searchData(data,',',offset);
		offset = searchData(data,',',offset);
		offset = offset + copyData(data+offset,new.cog,',');
		offset = offset + copyData(data+offset,new.date,',');
	}
	else
	{
		return new;
	}

	new.latConv = atoi(new.lat)/100 + atof(new.lat+2) /60;
	new.lonConv = atoi(new.lon)/100 + atof(new.lon+3) /60;

	return new;
}

int searchData(char data[], char until, int offset)
{
	int i = offset;
	while(*(data+i) != until)
	{
		i++;
	}
	return i+1;
}
int copyData(char data[], char dest[], char until)
{
	int i = 0;
	while(*(data+i) != until)
	{
		*(dest+i) = *(data+i);
		i++;
	}
	*(dest+i) = 0;
	return i+1;
}

float getBearing(float lat1, float lon1, float lat2, float lon2)
{
	float x = 0, y = 0 , b = 0;

	if(lat1 == lat2 && lon1 == lon2)
	{
		return -1;
	}

	x = cos(lat2*PI_180)*sin((lon2-lon1)*PI_180);
	y = cos(lat1*PI_180)*sin(lat2*PI_180) - sin(lat1*PI_180) * cos(lat2*PI_180) * cos((lon2-lon1)*PI_180);

	b = atan2(x,y)*_180_PI;
	return b;
}

float getDistance(float lat1, float lon1, float lat2, float lon2)
{
	float dlat = 0, dlon = 0, hav = 0;

	dlat = (lat2-lat1)*PI_180;
	dlon = (lon2-lon1)*PI_180;
	hav = pow(sin(dlat/2),2) + pow(sin(dlon/2),2) * cos(lat2*PI_180) * cos(lat1*PI_180);
	return 2* asin(sqrt(hav)) *	6371.0 * 1000.0;
}

int main(void)
{
	//RMC new = parseRMC(string);
	//RMC twee = parseRMC(string3);
	//float c = getDistance(new.latConv,new.lonConv,twee.latConv,twee.lonConv);
	float d = getBearing(52.080887, 5.114651,52.018853, 4.868433);
	//float h = getDistance(-90,0,90,0);
	float e = getBearing(52.080887, 5.114651,51.995068, 5.103414);
	float f = getBearing(52.080887, 5.114651,51.983766, 5.322306);


}
