/*
 * rmc.h
 *
 *  Created on: 22 nov. 2019
 *      Author: Vince
 */

#ifndef RMC_H_
#define RMC_H_

#include <math.h>

#define PI acos(-1.0)
#define PI_180 PI/180.0
#define _180_PI 180.0/PI

typedef struct
{
	char time[10];
	char date[7];
	char lat[11];
	char lon[12];
	char cog[6];
	float latConv;
	float lonConv;
} RMC;

RMC parseRMC(char data[]);
int searchData(char data[], char until, int offset);
int copyData(char data[], char dest[], char until);
float getBearing(float lat1, float lon1, float lat2, float lon2);
float getDistance(float lat1, float lon1, float lat2, float lon2);


#endif /* RMC_H_ */
