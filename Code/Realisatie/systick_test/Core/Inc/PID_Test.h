/*
 * PID_Test.h
 *
 *  Created on: 6 dec. 2019
 *      Author: ToBeEdited
 */

#ifndef SRC_PID_TEST_H_
#define SRC_PID_TEST_H_

#define leftEngine TIM5->CCR1
#define rightEngine TIM5->CCR2
#define servo TIM5->CCR3

typedef struct
{
	float enginePowerLimitLeft;
	float engineDivideFactorLeft;
	float enginePowerLimitRight;
	float engineDivideFactorRight;
}enginePower;

enginePower enginePowerLimit;

enum engineDirection {forward, forwardHalfThrottle, off, backward, rightTurn, leftTurn, rightForward, leftForward};

int16_t percentageToPWM(float PIDPercentage);
float degreeToPercentage(int16_t degree);
int16_t setTo360(int16_t degree);
int16_t determineError(int16_t setpointAngle, int16_t orientationAngle);
void percentageToPWMvoid(float PIDPercentage);
void calculatePID(float bearing, char* cog);
void engineMode(uint8_t engineMode);
void initEngine();

#endif /* SRC_PID_TEST_H_ */
