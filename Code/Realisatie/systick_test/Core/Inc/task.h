/*
 * task.h
 *
 *  Created on: 13 dec. 2019
 *      Author: Vince
 */

#ifndef TASK_H_
#define TASK_H_

enum taskState{RUNNING, READY, WAITING};

typedef struct TaskNode
{
	void (*p2Function)();
	int priority;
	int period;
	int counter;
	enum taskState state;
	struct TaskNode *next;
}Task;

typedef struct
{
	Task *head;
	Task *tail;
}Tasklist;


Tasklist *createList(void);
void addTask(Tasklist *list, int period, int priority,  void (*p2Function)());
void toggleLed(void);

#endif /* TASK_H_ */
