/*
 * gnssParse.h
 *
 *  Created on: Dec 19, 2019
 *      Author: Vince
 */

#ifndef INC_GNSSPARSE_H_
#define INC_GNSSPARSE_H_

#include <debug.h>
#include <rmc.h>
#include <route.h>
#include "main.h"

void initGnss(void);
RMC getGnssData(RMC data, Route *route);

#endif /* INC_GNSSPARSE_H_ */
