/*
 * schedule.h
 *
 *  Created on: 18 dec. 2019
 *      Author: Vince
 */

#ifndef SRC_SCHEDULE_H_
#define SRC_SCHEDULE_H_

#include <task.h>


void sleep(void);
Tasklist *nextTask(Tasklist *list);
void runCurrentTask(Task *task);

#endif /* SRC_SCHEDULE_H_ */
