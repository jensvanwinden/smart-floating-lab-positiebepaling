/*
 * rmc.h
 *
 *  Created on: 22 nov. 2019
 *      Author: Vince
 */

#ifndef RMC_H_
#define RMC_H_

#include <math.h>
#include "main.h"

#define PI acos(-1.0)
#define PI_180 PI/180.0
#define _180_PI 180.0/PI
#define NMEA_LENGHT_NO_FIX 68 //40
#define NMEA_LENGHT_FIX 72

UART_HandleTypeDef huart2;

typedef struct
{
	char time[10];
	char date[7];
	char lat[11];
	char lon[12];
	char cog[6];
	char valid[2];
	float latConv;
	float lonConv;
	float bearing;
	float haversineDist;
} RMC;

RMC parseRMC(char data[]);
int searchData(char data[], char until, int offset);
int copyData(char data[], char dest[], char until);
float getBearing(float lat1, float lon1, float lat2, float lon2);
float getDistance(float lat1, float lon1, float lat2, float lon2);
void getRawRMC(char data[]);
void MX_USART2_UART_Init(void);


#endif /* RMC_H_ */
