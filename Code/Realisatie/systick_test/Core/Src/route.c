/*
 * route.c
 *
 *  Created on: 5 dec. 2019
 *      Author: Vince
 */

#include <debug.h>
#include <route.h>
#include <lora.h>
#include <stddef.h>
#include <stdlib.h>

Route *createRoute(void)
{
	Route *new = malloc(sizeof(Route));
	new->end = NULL;
	new->currentLocation = NULL;
	return new;
}


void addLocation(Route *actual, float lat, float lon)
{
	Location *new = malloc(sizeof(Location));
	new->lat = lat;
	new->lon = lon;
	new->next = NULL;
	if(actual->currentLocation == NULL)
	{
		actual->currentLocation = actual->end = new;
	}
	else
	{
		actual->end = actual->end->next = new;
	}
}

uint8_t arrived(Route *route)
{
	static int i = 0;
	if(route->currentLocation == route->end)
	{
		return 1;
	}
	route->currentLocation = route->currentLocation->next;
	i++;
	payload.arrived_spots = i;
	return 0;
}

//int main(void)
//{
//	Route *route = createRoute();
//	addLocation(route, 1,2);
//	addLocation(route, 2,2);
//	addLocation(route, 3,2);
//	arrived(route);
//	arrived(route);
//	arrived(route);
//	arrived(route);
//}

