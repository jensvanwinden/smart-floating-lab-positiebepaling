/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/

#include "schedule.h"
#include "main.h"




Tasklist *nextTask(Tasklist *list)
{
	Task *loop = list->head;
	Task *prio = {0};
	prio->priority = 11;
	while(loop != NULL)
	{
		if(loop->state == READY && prio->priority > loop->priority)
		{
			prio = loop;
		}
		loop = loop->next;
	}
	return prio;
}

void runCurrentTask(Task *task)
{
	task->p2Function();
	task->state = WAITING;
	task->counter = task->period;
}

