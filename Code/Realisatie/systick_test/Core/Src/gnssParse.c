#include <gnssParse.h>
#include "lora.h"




RMC getGnssData(RMC data, Route *route)
{
	char in[144] = {0};
  	getRawRMC(in);

	//debugCharPrint(in);
  	//if(in[0] == '$')
  //	{
  		data = parseRMC(in);
  //	}
	data.bearing = getBearing(data.latConv, data.lonConv, route->currentLocation->lat, route->currentLocation->lon);
	data.haversineDist = getDistance(data.latConv, data.lonConv, route->currentLocation->lat, route->currentLocation->lon);
	payload.lat = data.latConv;
	payload.lon = data.lonConv;
	payload.distance = data.haversineDist;
	return data;
}
