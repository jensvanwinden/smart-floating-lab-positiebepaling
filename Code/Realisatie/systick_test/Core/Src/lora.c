/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "debug.h"
#include "lora.h"
#include <string.h>
#include "rmc.h"


/* USER CODE BEGIN Includes */

#include <stdio.h>
#include "SX1278.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
/* USER CODE END PV */
void MX_GPIO_Init(void);
void MX_SPI1_Init(void);
void MX_USART6_UART_Init(void);
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/


SX1278_hw_t SX1278_hw;
SX1278_t SX1278;
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */



void initLora(void)
{
	SX1278_hw.dio0.port = DIO0_GPIO_Port;
	SX1278_hw.dio0.pin = DIO0_Pin;
	SX1278_hw.nss.port = NSS_GPIO_Port;
	SX1278_hw.nss.pin = NSS_Pin;
	SX1278_hw.reset.port = RESET_LORA_GPIO_Port;
	SX1278_hw.reset.pin = RESET_LORA_Pin;
	SX1278_hw.spi = &hspi1;

	SX1278.hw = &SX1278_hw;

	debugCharPrint("Configuring LoRa module\r\n");
	SX1278_begin(&SX1278, SX1278_868_5MHZ, SX1278_POWER_17DBM, SX1278_LORA_SF_7, SX1278_LORA_BW_125KHZ,100);
	debugCharPrint("Done configuring LoRaModule\r\n");
}

void initReceive(void)
{
	ret = SX1278_LoRaEntryRx(&SX1278, 100, 2000);
}

char* receiveString(void)
{
	HAL_Delay(100);

	ret = SX1278_LoRaRxPacket(&SX1278);

	if (ret > 0)
	{
		debugCharPrint("Package received ...");
		debugPrintEnter();
		SX1278_read(&SX1278, (uint8_t*) payloadBuffer, ret);
		debugCharPrint("Bytes received: ");
		debugIntPrint(ret);
		debugPrintEnter();
		//debugCharPrint(buffer);
		int offset = 0;
		char value[10];
		char parameters[16][15] = {"Distance:", "Bearing:", "COG:", "Lat:", "Lon:", "Pterm:", "Iterm:", "Dterm:", "errorAngle:", "PIDperc:", "MotorL:", "MotorR:", "Servo:", "SampleTime:", "Destination:", "Arrived_spots:" };
		for(int i = 0; i<16; i++)
		{
			offset = offset + copyData(payloadBuffer+offset,value,' ');

			debugCharPrint(parameters[i]);
			debugCharPrint(value);
			debugPrintEnter();


		}

		HAL_GPIO_TogglePin(LED_D1_GPIO_Port, LED_D1_Pin);

		//check the CRC
		if (SX1278_SPIRead(&SX1278, LR_RegHopChannel) && 0x20) //if a message with crc is received
		{
			if (SX1278_SPIRead(&SX1278, LR_RegIrqFlags) && 0x20) //if the PayloadCrcError is high
			{
				debugCharPrint("CRC_ERROR");
				SX1278_SPIWrite(&SX1278, LR_RegIrqFlags, 0x20); //clear the flag
			}
			else
			{
				debugCharPrint("CRC_GOOD");
			}
			debugPrintEnter();debugPrintEnter();
			char *ptr = payloadBuffer;
			return ptr;
		}
	}
	return 0;
}

void sendString(char message[])
{
			//debugCharPrint("Master ...");
			//HAL_Delay(2500);
			//debugCharPrint("Sending package...");

			message_length = sprintf(buffer, message);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
			ret = SX1278_LoRaEntryTx(&SX1278, message_length, timeOut);
//			debugCharPrint("Entry: ");
//			debugCharPrint("Sending ");
//			debugCharPrint(buffer);

			ret = SX1278_LoRaTxPacket(&SX1278, (uint8_t *) buffer, message_length, timeOut);


			if (ret)
			{
				//debugCharPrint("Transmission successful...");
				HAL_GPIO_TogglePin(LED_D1_GPIO_Port, LED_D1_Pin);
			}
			else
			{
				//debugCharPrint("Transmission failed...");

			}
}

void payload2string(payloadTag *payload)
{
	char tmpstr[20];
	memset(&payloadString[0], 0, sizeof(payloadString));

	sprintf(tmpstr, "%d ", payload->distance);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->bearing);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->cog);
	strcat(payloadString, tmpstr);

	sprintf(tmpstr, "%.09f ", payload->lat);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.09f ", payload->lon);
	strcat(payloadString, tmpstr);

	sprintf(tmpstr, "%.03f ", payload->PTerm);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->ITerm);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->DTerm);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->errorAngle);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->PIDperc);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->MotorL);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->MotorR);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->Servo);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%.03f ", payload->SampleTime);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->destination);
	strcat(payloadString, tmpstr);
	sprintf(tmpstr, "%d ", payload->arrived_spots);
	strcat(payloadString, tmpstr);
}
void setting2string(payloadTag *payload)
{
	char tmpstr[20];
	memset(&settingString[0], 0, sizeof(settingString));

	sprintf(tmpstr, "%d ", payload->reset);
	strcat(settingString, tmpstr);
	sprintf(tmpstr, "%d ", payload->callback);
	strcat(settingString, tmpstr);
	sprintf(tmpstr, "%d ", payload->variable);
	strcat(settingString, tmpstr);

	sprintf(tmpstr, "%.04f ", payload->value);
	strcat(settingString, tmpstr);
}

void receiveSettings(void)
{
	HAL_Delay(1000);

	ret = SX1278_LoRaRxPacket(&SX1278);

	if (ret > 0)
	{
		debugCharPrint("Package received ...");
		debugPrintEnter();
		SX1278_read(&SX1278, (uint8_t*) settingsBuffer, ret);
		debugCharPrint("Bytes received: ");
		debugIntPrint(ret);
		debugPrintEnter();

		int offset = 0;
		char value[10];
		char parameters[4][13] = { "Reset: ", " Callback: ", " Variable: ", " Value: "};

		for (int i = 0; i < 4; i++)
		{
			offset = offset + copyData(settingsBuffer + offset, value, ' ');
			debugCharPrint(parameters[i]);
			debugCharPrint(value);
		}
		debugPrintEnter();debugPrintEnter();
	}

}
