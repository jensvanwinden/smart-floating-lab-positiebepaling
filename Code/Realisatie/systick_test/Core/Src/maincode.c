/*
 * maincode.c
 *
 *  Created on: 23 dec. 2019
 *      Author: Vince
 */
#include <gnssParse.h>
#include <lora.h>
#include "main.h"
#include "schedule.h"
#include "PID_Test.h"
#include "PID_init.h"
#include "JSN-SR04T.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
TIM_HandleTypeDef htim11;
SPI_HandleTypeDef hspi1;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
volatile int flag = 0;
Tasklist *list;


RMC new = {{0}, {0}, {0}, {0}, {0}, {0}, 0, 0, 0, 0};
Route *route;
float newTime = 0;
float lastTime = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM11_Init(void);
void MX_SPI1_Init(void);


/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
    list = createList();
	addTask(list, 125, 2, GNSS);
	addTask(list, 125, 3, PID);
	addTask(list, 1000, 1, loraSend); //1500
	//addTask(list, 1100, 3, loraReceive);

	addTask(list, 1, 10, sleep);

  /* USER CODE END 1 */


	/* USER CODE BEGIN 2 */
	route = createRoute();

  addLocation(route, 52.017858, 4.422356); //einde kippenhok

//  test rondje kort (driehoek), korte filmpje van michael bij het klimrek
//	addLocation(route, 52.035005, 4.402751);
//	addLocation(route, 52.035135, 4.403215);
//	addLocation(route, 52.035254, 4.402910);

//  test rondje zigzag 5 punten afgaan
//	addLocation(route, 52.035133, 4.402883);
//	addLocation(route, 52.035171, 4.403057);
//	addLocation(route, 52.035133, 4.402883);
//	addLocation(route, 52.035179, 4.402926);
//	addLocation(route, 52.035254, 4.402910);

//  test lange afstand, rond 350M totaal afgelegd, lange filmpje michael
//	addLocation(route, 52.035222, 4.403280);
//	addLocation(route, 52.035153, 4.403752);
//	addLocation(route, 52.034839, 4.404793);
//	addLocation(route, 52.034727, 4.405262);
//	addLocation(route, 52.035254, 4.402910);

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_USART6_UART_Init();
  MX_USART1_UART_Init();

  MX_SPI1_Init();


  /* USER CODE BEGIN 2 */
  PID_init();
  initLora();
  initEngine();
  MX_TIM11_Init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

    /* USER CODE END WHILE */
	  //static uint16_t distance;
	  //distance = detectObstacle();
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */


void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, NSS_Pin|GPIO_PIN_11, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, RESET_LORA_Pin|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_D1_Pin|GPIO_PIN_15|task3_Pin|task2_Pin
                          |ONEWIRE_DEBUG_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : NSS_Pin PA11 */
  GPIO_InitStruct.Pin = NSS_Pin|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : RESET_LORA_Pin PC8 */
  GPIO_InitStruct.Pin = RESET_LORA_Pin|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_D1_Pin PB15 task3_Pin task2_Pin
                           ONEWIRE_DEBUG_Pin */
  GPIO_InitStruct.Pin = LED_D1_Pin|GPIO_PIN_15|task3_Pin|task2_Pin
                          |ONEWIRE_DEBUG_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : DIO0_Pin */
  GPIO_InitStruct.Pin = DIO0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(DIO0_GPIO_Port, &GPIO_InitStruct);

}

static void MX_TIM11_Init(void)
{

  /* USER CODE BEGIN TIM11_Init 0 */

  /* USER CODE END TIM11_Init 0 */

  /* USER CODE BEGIN TIM11_Init 1 */

  /* USER CODE END TIM11_Init 1 */
  htim11.Instance = TIM11;
  htim11.Init.Prescaler = 2;
  htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim11.Init.Period = 33705; //33332
  htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim11.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim11) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM11_Init 2 */
  TIM11->EGR = 1;
  HAL_TIM_Base_Start(&htim11);
  HAL_TIM_Base_Start_IT(&htim11);

  /* USER CODE END TIM11_Init 2 */

}

void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 15;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/* USER CODE BEGIN 4 */
void GNSS(void)
{
//	HAL_TIM_Base_Stop_IT(&htim11);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
//	HAL_GPIO_TogglePin(ONEWIRE_DEBUG_GPIO_Port, ONEWIRE_DEBUG_Pin);
	new = getGnssData(new, route);
	if(new.haversineDist <= 2)
	{
	engineMode(off);
	HAL_Delay(5000);
	flag = arrived(route);
	//HAL_Delay(10000);

	  if(flag == 1)
	  {
		  payload.destination = 1;
		  //addLocation(route, 52.017615, 4.422524);
	//					  debugCharPrint("Eindbestemming bereikt");
	//					  debugPrintEnter();
	  }
	}
//	HAL_GPIO_TogglePin(ONEWIRE_DEBUG_GPIO_Port, ONEWIRE_DEBUG_Pin);

	newTime += 0.210;
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
//	HAL_TIM_Base_Start_IT(&htim11);

}

void loraSend(void)
{

	//HAL_TIM_Base_Stop_IT(&htim11);

	HAL_GPIO_TogglePin(task3_GPIO_Port, task3_Pin);
	payload2string(&payload); //convert the payload struct to a string. both are global


	sendString(payloadString); //send the payload over lora
	HAL_GPIO_TogglePin(task3_GPIO_Port, task3_Pin);
	newTime += 0.262;

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
	//HAL_GPIO_TogglePin(ONEWIRE_DEBUG_GPIO_Port, ONEWIRE_DEBUG_Pin);
	//HAL_TIM_Base_Start_IT(&htim11);

}

void loraReceive(void)
{
	//HAL_GPIO_TogglePin(ONEWIRE_DEBUG_GPIO_Port, ONEWIRE_DEBUG_Pin);
	//initReceive();
	//receiveString();

	//HAL_GPIO_TogglePin(ONEWIRE_DEBUG_GPIO_Port, ONEWIRE_DEBUG_Pin);
}

void PID(void)
{
	HAL_GPIO_TogglePin(task2_GPIO_Port, task2_Pin);

	int16_t	debugCog = atoi(new.cog);
	payload.cog = debugCog;

	debugCog = 0;

	if(debugCog != 0)
	{
		calculatePID(new.bearing, new.cog);
	}
	else
	{
		engineMode(forward);
		servo = 150;
		payload.Servo = servo;
		payload.MotorL = leftEngine;
		payload.MotorR = rightEngine;
	}
	HAL_GPIO_TogglePin(task2_GPIO_Port, task2_Pin);
	//HAL_GPIO_TogglePin(ONEWIRE_DEBUG_GPIO_Port, ONEWIRE_DEBUG_Pin);
}

void sleep(void)
{
	//HAL_GPIO_TogglePin(ONEWIRE_DEBUG_GPIO_Port, ONEWIRE_DEBUG_Pin);

}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
