/*
 * task.c
 *
 *  Created on: 13 dec. 2019
 *      Author: Vince
 */

#include "task.h"
#include <stdlib.h>



Tasklist *createList(void)
{
	Tasklist *new = malloc(sizeof(Tasklist));
	new->head = NULL;
	new->tail = NULL;
	return new;
}

void addTask(Tasklist *list, int counter, int priority,  void (*p2Function)())
{
	Task *new = malloc(sizeof(Task));
	new->counter = counter;
	new->p2Function = p2Function;
	new->priority = priority;
	new->state = READY;
	if(list->head == NULL)
	{
		list->head = list->tail = new;
	}
	else
	{
		list->tail = list->tail->next = new;
	}
}


